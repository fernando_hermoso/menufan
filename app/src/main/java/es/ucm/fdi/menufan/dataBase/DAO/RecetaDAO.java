package es.ucm.fdi.menufan.dataBase.DAO;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import es.ucm.fdi.menufan.dataBase.Entities.RecetaEntity;
@Dao
public interface RecetaDAO {


    //Return= todas las recetas
    @Query("select * from Receta")
    List<RecetaEntity> getAll();

    //Return= una receta
    @Query("select * from Receta where Receta = :recetaId")
    RecetaEntity findById(String recetaId);

    @Insert
    void insert (RecetaEntity receta);

    @Update
    void update (RecetaEntity receta);

    @Delete
    void delete (RecetaEntity receta);

}
