package es.ucm.fdi.menufan.dataBase.DAO;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import es.ucm.fdi.menufan.dataBase.Entities.ProductoEntity;

@Dao
public interface ProductoDAO {

    //Return= todos los productos
    @Query("select * from Producto")
    List<ProductoEntity> getAll();

    //Return= una producto
    @Query("select * from Producto where producto = :productId")
    ProductoEntity findById(String productId);

    @Insert
    void insert (ProductoEntity producto);

    @Update
    void update (ProductoEntity producto);

    @Delete
    void delete (ProductoEntity producto);
}
