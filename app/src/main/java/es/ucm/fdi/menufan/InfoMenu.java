package es.ucm.fdi.menufan;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import es.ucm.fdi.menufan.adapters.MenuRecetasAdapter;
import es.ucm.fdi.menufan.adapters.RecetasProductosAdapter;
import es.ucm.fdi.menufan.dataBase.AppDataBase;
import es.ucm.fdi.menufan.dataBase.DAO.RecetaMenuDAO;
import es.ucm.fdi.menufan.dataBase.Entities.ProductoRecetasEntity;
import es.ucm.fdi.menufan.dataBase.Entities.RecetaMenuEntity;

public class InfoMenu extends AppCompatActivity {

    /*
     * CLASE QUE SE MUESTRA AL PULSAR A UNA LSITA DE LA REceta, EN ESTA TE MUESTRA SUS PRODUCTOS
     * */

    private static final int REGISTER_RECETAS_REQUEST = 2;
    TextView nombreMenu;
    RecyclerView recyclerView;
    MenuRecetasAdapter menuRecetasAdapter;
    Button addReceta;
    Context context;
    List<RecetaMenuEntity> listaRecetas;
    List<String> listaLocal;
    //List<String>listaRegistroMenus;

    AppDataBase dataBase;
    RecetaMenuDAO recetaMenuDAO;

    BottomNavigationView navigation;

    String menu;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_menu);

        init();
    }

    @Override
    protected void onResume () {
        super.onResume();
        listaRecetas = recetaMenuDAO.findById(menu);
        menuRecetasAdapter.setListaProductosRecetas(listaRecetas);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REGISTER_RECETAS_REQUEST){
            if(resultCode == RESULT_OK){


                listaLocal = (List<String>) data.getSerializableExtra("listaRegistroRecetas");

                for(String i: listaLocal){

                    if (!buscarElemento(i)){
                        RecetaMenuEntity nuevaReceta = new RecetaMenuEntity(menu,i);
                        recetaMenuDAO.insert(nuevaReceta);


                    //
                    //    menuRecetasAdapter.setListaProductosRecetas(listaRecetas);
                    }

                }
                listaLocal.clear();
                listaRecetas = recetaMenuDAO.findById(menu);
                menuRecetasAdapter.setListaProductosRecetas(listaRecetas);

            }else if(resultCode == RESULT_CANCELED){
                Log.d("Error", "Ha ocurridor error en cargar el registro de productos");
            }

        }

    }

    private Boolean buscarElemento (String idReceta){

        Boolean encontrado = false;
        int j = 0;
        while (!encontrado && j < listaRecetas.size()){
            if (listaRecetas.get(j).getRecetaId().equals(idReceta)){
                encontrado = true;
            }
            j++;
        }
        return encontrado;
    }

    private void init(){
        // Id menu
        menu = getIntent().getStringExtra("NombreMenu");

        nombreMenu = findViewById(R.id.menu);
        recyclerView = findViewById(R.id.RecyclerView);
        addReceta = findViewById(R.id.botonFLotante);
        context = this;

        menuRecetasAdapter = new MenuRecetasAdapter(context);
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        recyclerView.setAdapter(menuRecetasAdapter);

        nombreMenu.setText(menu);

        listaLocal = new ArrayList<>();
        //listaRegistroMenus = new ArrayList<>();

        //DataBase
        dataBase = AppDataBase.getInstanceDataBase(this.getApplicationContext());
        recetaMenuDAO = dataBase.recetaMenuDAO();

        addReceta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AddRecetasLista.class);
                intent.putExtra("listaRegistroRecetas",(Serializable)listaLocal);
                startActivityForResult(intent,REGISTER_RECETAS_REQUEST);
            }
        });


        navigation = findViewById(R.id.menu_bottom_navigation);
        navigation.setSelectedItemId(R.id.menu_menu);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.menu_gastos:
                        startActivity(new Intent(getApplicationContext(), Gastos.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.menu_list:
                        startActivity(new Intent(getApplicationContext(), ListaCompras.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.menu_menu:
                        startActivity(new Intent(getApplicationContext(), Menus.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.menu_recetas:
                        startActivity(new Intent(getApplicationContext(), Recetas.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.menu_productos:
                        startActivity(new Intent(getApplicationContext(), Productos.class));
                        overridePendingTransition(0, 0);
                        return true;

                }
                return false;
            }
        });

    }
}