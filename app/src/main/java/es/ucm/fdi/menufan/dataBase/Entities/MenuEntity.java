package es.ucm.fdi.menufan.dataBase.Entities;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;


@Entity(tableName = "Menu")
public class MenuEntity {

    public MenuEntity() {
    }

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "MenuID")
    private String NombreMenuID;


    public MenuEntity(@NonNull String nombreMenuID) {
        NombreMenuID = nombreMenuID;
    }

    @NonNull
    public String getNombreMenuID() {
        return NombreMenuID;
    }

    public void setNombreMenuID(@NonNull String nombreMenuID) {
        NombreMenuID = nombreMenuID;
    }
}
