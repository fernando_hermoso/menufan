package es.ucm.fdi.menufan.dataBase.Entities;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity( tableName = "PrecioLista",
        foreignKeys = {
                        @ForeignKey(entity = PrecioEntity.class,
                        parentColumns = "PrecioId",
                        childColumns = "precioIdLista",
                        onDelete = ForeignKey.CASCADE,
                        onUpdate = ForeignKey.CASCADE)
        }
)
public class PrecioListaEntity {


    public PrecioListaEntity() {
    }

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "precioIdLista")
    private Integer PrecioId;

    @ColumnInfo(name = "listaId")
    private String NombreListaID;


    public PrecioListaEntity(@NonNull Integer precioId, String nombreListaID) {
        PrecioId = precioId;
        NombreListaID = nombreListaID;
    }

    @NonNull
    public Integer getPrecioId() {
        return PrecioId;
    }

    public void setPrecioId(@NonNull Integer precioId) {
        PrecioId = precioId;
    }

    public String getNombreListaID() {
        return NombreListaID;
    }

    public void setNombreListaID(String nombreListaID) {
        NombreListaID = nombreListaID;
    }
}
