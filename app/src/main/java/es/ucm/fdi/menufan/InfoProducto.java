package es.ucm.fdi.menufan;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InfoProducto extends AppCompatActivity {

    /*
     * CLASE QUE SE MUESTRA AL PULSAR UN PRODUCTO
     * */

    String nombreProducto;
    String URL = "https://api.spoonacular.com/food/ingredients/";
    String id;
    TextView nombre;
    TextView calorias;
    TextView grasas;
    TextView saturadas;
    TextView azucar;
    TextView carbohidratos;
    TextView proteinas;
    Button atras;
    List<ProductoID> list;
    Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_producto);
        init();
        id = buscaId(list);

        URL+= id+"/information?apiKey=e95ddbc60fa24991bb70480291510571&amount=100";
        getProductInformation();

    }

    private String buscaId(List<ProductoID> list) {
        boolean encotrado = false;
        String id = "";
        int i = 0;
        while (!encotrado && i<list.size()){

            if(list.get(i).getNombre().equals(nombreProducto)){
                id = list.get(i).getId();
                encotrado = true;
            }
            i++;
        }
        return id;
    }


    private void init(){

        nombreProducto = getIntent().getStringExtra("NombreProducto");
        list = Productos.listaIDs;
        nombre = findViewById(R.id.NombreProducto);
        nombre.setText(nombreProducto);
        context = this;
        calorias = findViewById(R.id.calorias);
        grasas = findViewById(R.id.grasas);
        saturadas = findViewById(R.id.saturadas);
        azucar = findViewById(R.id.azucar);
        carbohidratos = findViewById(R.id.carbohidratos);
        proteinas = findViewById(R.id.proteina);
        atras = findViewById(R.id.atras);
        atras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, Productos.class);
                startActivity(intent);
            }
        });

    }


    private void getProductInformation() {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, URL, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.d("TAG", "funciona");
                            JSONObject nutricion = response.getJSONObject("nutrition");
                            JSONArray res = nutricion.getJSONArray("nutrients");

                            int tam = res.length();
                            for (int i = 0; i< tam; i++){
                                JSONObject info = res.getJSONObject(i);
                                String name = info.getString("name");
                                String amount = info.getString("amount");
                                String unit = info.getString("unit");


                                if(name.equals("Calories")){
                                    String resultado = amount+ " " + unit;
                                    calorias.setText(resultado);
                                }
                                if(name.equals("Fat")){
                                    String resultado = amount+ " " + unit;
                                    grasas.setText(resultado);
                                }
                                if(name.equals("Saturated Fat")){
                                    String resultado = amount+ " " + unit;
                                    saturadas.setText(resultado);
                                }
                                if(name.equals("Sugar")){
                                    String resultado = amount+ " " + unit;
                                    azucar.setText(resultado);
                                }
                                if(name.equals("Carbohydrates")){
                                    String resultado = amount+ " " + unit;
                                    carbohidratos.setText(resultado);
                                }
                                if(name.equals("Protein")){
                                    String resultado = amount+ " " + unit;
                                    proteinas.setText(resultado);
                                }

                            }




                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof ServerError) {
                            Log.d("TAG", "Error en servidor");
                        }
                        if (error instanceof NoConnectionError) {//no conexion internet
                            Log.d("TAG", "No hay conexion a internet");
                        }

                        if (error instanceof NetworkError) {//desconexionsocket
                            Log.d("TAG", "NetWorkError");
                        }

                    }
                }){
/*
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                String credentials = "ipallas@ucm.es:MisShavale8432!";

                String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Authorization", auth);


                return headers;
            }*/
        };

        VolleySingleton.getInstaceVolley(getApplicationContext()).addToRequestQueue(request);
    }


}