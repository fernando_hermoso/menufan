package es.ucm.fdi.menufan.dataBase.Entities;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;

@Entity(tableName = "ListacompraProducto",
        primaryKeys = {"listaID", "ProductoListaID"},
        foreignKeys = {
                        @ForeignKey(
                                entity = ListaCompraEntity.class,
                                parentColumns = "NombreListaID",
                                childColumns = "listaID",
                                onDelete = ForeignKey.CASCADE,
                                onUpdate = ForeignKey.CASCADE
                        ),

                        @ForeignKey(
                                entity = ProductoEntity.class,
                                parentColumns = "producto",
                                childColumns = "ProductoListaID",
                                onDelete = ForeignKey.CASCADE,
                                onUpdate = ForeignKey.CASCADE)

        })
public class ListacompraProductoEntity {

    @NonNull
    @ColumnInfo(name = "listaID")
    private String nombreListaId;

    @NonNull
    @ColumnInfo(name = "ProductoListaID")
    private String nombreProducto;

    @ColumnInfo(name = "cantidad")
    private Integer cantidad;


    public ListacompraProductoEntity() { }

    public ListacompraProductoEntity(@NonNull String nombreListaId, @NonNull String nombreProducto, Integer cantidad) {
        this.nombreListaId = nombreListaId;
        this.nombreProducto = nombreProducto;
        this.cantidad = cantidad;
    }

    @NonNull
    public String getNombreListaId() {
        return nombreListaId;
    }

    public void setNombreListaId(@NonNull String nombreListaId) {
        this.nombreListaId = nombreListaId;
    }

    @NonNull
    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(@NonNull String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }
}
