package es.ucm.fdi.menufan.dataBase.DAO;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import es.ucm.fdi.menufan.dataBase.Entities.ProductoRecetasEntity;

@Dao
public interface ProductoRecetaDAO {

    //Return= todas los productos con sus recetas asociadas
    @Query("select * from ProductoRecetas")
    List<ProductoRecetasEntity> getAll();

    //Return= Todos los productos de uan receta
    @Query("select * from ProductoRecetas where recetaID = :recetID")
    List<ProductoRecetasEntity> findById(String recetID);

    @Insert
    void insert (ProductoRecetasEntity lista);

    @Update
    void update (ProductoRecetasEntity lista);

    @Delete
    void delete (ProductoRecetasEntity lista);
}
