package es.ucm.fdi.menufan;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import es.ucm.fdi.menufan.adapters.ListaProductosAdapter;
import es.ucm.fdi.menufan.adapters.RecetasProductosAdapter;
import es.ucm.fdi.menufan.dataBase.AppDataBase;
import es.ucm.fdi.menufan.dataBase.DAO.ProductoRecetaDAO;
import es.ucm.fdi.menufan.dataBase.Entities.ListaCompraEntity;
import es.ucm.fdi.menufan.dataBase.Entities.ListacompraProductoEntity;
import es.ucm.fdi.menufan.dataBase.Entities.ProductoRecetasEntity;
import es.ucm.fdi.menufan.dataBase.Entities.RecetaMenuEntity;

public class InfoRecetas extends AppCompatActivity {

    /*
     * CLASE QUE SE MUESTRA AL PULSAR A UNA LSITA DE LA REceta, EN ESTA TE MUESTRA SUS PRODUCTOS
     * */

    private static final int REGISTER_PRODUCTOS_REQUEST = 1;
    TextView nombreReceta;
    TextView descripcion;
    RecyclerView recyclerView;
    RecetasProductosAdapter listaProductosAdapter;
    Button addProducto;
    Context context;
    List<ProductoRecetasEntity> listaProducto;
    List<String> listaLocal;
    BottomNavigationView navigation;
    String receta;


    AppDataBase dataBase;
    ProductoRecetaDAO productoRecetaDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_recetas);

        init();
    }

    @Override
    protected void onResume () {
        super.onResume();
        listaProducto = productoRecetaDAO.findById(receta);
        listaProductosAdapter.setListaProductosRecetas(listaProducto);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REGISTER_PRODUCTOS_REQUEST){
            if(resultCode == RESULT_OK){


                listaLocal = (List<String>) data.getSerializableExtra("listaRegistroProductos");

                for(String i: listaLocal){

                    if (!buscarElemento(i)){
                        ProductoRecetasEntity nuevoProducto = new ProductoRecetasEntity(i,receta);
                        productoRecetaDAO.insert(nuevoProducto);

                    }

                }
                listaLocal.clear();
                listaProducto = productoRecetaDAO.findById(receta);
                listaProductosAdapter.setListaProductosRecetas(listaProducto);

            }else if(resultCode == RESULT_CANCELED){
                Log.d("Error", "Ha ocurridor error en cargar el registro de productos");
            }

        }

    }

    private Boolean buscarElemento (String idProducto){

        Boolean encontrado = false;
        int j = 0;
        while (!encontrado && j < listaProducto.size()){
            if (listaProducto.get(j).getProductoId().equals(idProducto)){
                encontrado = true;
            }
            j++;
        }
        return encontrado;
    }


    private void init(){

        receta = getIntent().getStringExtra("NombreReceta");// receta ID
        nombreReceta = findViewById(R.id.receta);
        descripcion = findViewById(R.id.Descripcion);
        recyclerView = findViewById(R.id.recyclerView);
        addProducto = findViewById(R.id.botonFLotante);
        listaProductosAdapter = new RecetasProductosAdapter(this);
        context = this;

        listaProducto = new ArrayList<>();
        listaLocal = new ArrayList<>();

        nombreReceta.setText(receta);
        descripcion.setText(getIntent().getStringExtra("DescripcionReceta"));

        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        recyclerView.setAdapter(listaProductosAdapter);

        //DataBase
        dataBase = AppDataBase.getInstanceDataBase(this.getApplicationContext());
        productoRecetaDAO = dataBase.productoRecetaDAO();

        addProducto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AddProducto.class);
                intent.putExtra("listaRegistroProductos",(Serializable)listaLocal);
                startActivityForResult(intent,REGISTER_PRODUCTOS_REQUEST);
            }
        });


        navigation = findViewById(R.id.menu_bottom_navigation);
        navigation.setSelectedItemId(R.id.menu_menu);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.menu_gastos:
                        startActivity(new Intent(getApplicationContext(), Gastos.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.menu_list:
                        startActivity(new Intent(getApplicationContext(), ListaCompras.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.menu_menu:
                        startActivity(new Intent(getApplicationContext(), Menus.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.menu_recetas:
                        startActivity(new Intent(getApplicationContext(), Recetas.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.menu_productos:
                        startActivity(new Intent(getApplicationContext(), Productos.class));
                        overridePendingTransition(0, 0);
                        return true;

                }
                return false;
            }
        });

    }



}