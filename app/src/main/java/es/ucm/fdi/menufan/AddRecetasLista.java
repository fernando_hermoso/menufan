package es.ucm.fdi.menufan;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import es.ucm.fdi.menufan.adapters.AddRecetaAdapter;
import es.ucm.fdi.menufan.dataBase.AppDataBase;
import es.ucm.fdi.menufan.dataBase.DAO.ProductoRecetaDAO;
import es.ucm.fdi.menufan.dataBase.DAO.RecetaDAO;
import es.ucm.fdi.menufan.dataBase.Entities.RecetaEntity;

public class AddRecetasLista extends AppCompatActivity implements QuantityListener {


    Button aniadirSelecion;
    Context context;

    AppDataBase dataBase;
    RecetaDAO recetaDAO;

    AddRecetaAdapter addRecetaAdapter;
    RecyclerView recyclerView;

    List<RecetaEntity> listaRecetas;
    List<String> recetasLocal;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_recetas_lista);

        init();
    }

    @Override
    protected void onResume () {
        super.onResume();
       listaRecetas = recetaDAO.getAll();
       addRecetaAdapter.setListaProductos(listaRecetas);

    }



    private void init(){

        context = this;
        recyclerView = findViewById(R.id.addRecetasRecycleview);
        aniadirSelecion = findViewById(R.id.addSeleccionados);

        listaRecetas = new ArrayList<>();
        recetasLocal = new ArrayList<>();

        addRecetaAdapter = new AddRecetaAdapter(recetasLocal,this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        recyclerView.setAdapter(addRecetaAdapter);


        dataBase = AppDataBase.getInstanceDataBase(this.getApplicationContext());
        recetaDAO = dataBase.recetaDAO();



        // Obtengo los datos del intent
        Intent intent = getIntent();
        recetasLocal = (List<String>) intent.getSerializableExtra("listaRegistroRecetas");

        aniadirSelecion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentRespuesta = new Intent();
                intentRespuesta.putExtra("listaRegistroRecetas", (Serializable) recetasLocal);
                setResult(RESULT_OK,intentRespuesta);
                finish();
            }
        });

    }

    @Override
    public void arrayRegistroProductos(List<String> list) {
        recetasLocal = list;
    }
}