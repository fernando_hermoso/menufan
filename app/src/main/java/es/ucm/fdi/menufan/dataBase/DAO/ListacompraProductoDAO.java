package es.ucm.fdi.menufan.dataBase.DAO;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import es.ucm.fdi.menufan.dataBase.Entities.ListacompraProductoEntity;

@Dao
public interface ListacompraProductoDAO {

    //Return= todas las listadeCompra
    @Query("select * from ListacompraProducto")
    List<ListacompraProductoEntity> getAll();

    //Return= una listaCompra con productos
    @Query("select * from ListacompraProducto where listaID = :listId")
    List<ListacompraProductoEntity> findById(String listId);

    @Insert
    void insert (ListacompraProductoEntity lista);

    @Update
    void update (ListacompraProductoEntity lista);

    @Delete
    void delete (ListacompraProductoEntity lista);
}
