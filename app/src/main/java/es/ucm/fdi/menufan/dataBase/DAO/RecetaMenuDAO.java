package es.ucm.fdi.menufan.dataBase.DAO;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import es.ucm.fdi.menufan.dataBase.Entities.RecetaMenuEntity;

@Dao
public interface RecetaMenuDAO {

    //Return= todas la recetasd con el menu asociado
    @Query("select * from RecetaMenu")
    List<RecetaMenuEntity> getAll();

    //Return= Todos las decetas dado un menu
    @Query("select * from RecetaMenu where menuId = :menID")
    List<RecetaMenuEntity> findById(String menID);

    @Insert
    void insert (RecetaMenuEntity lista);

    @Update
    void update (RecetaMenuEntity lista);

    @Delete
    void delete (RecetaMenuEntity lista);
}
