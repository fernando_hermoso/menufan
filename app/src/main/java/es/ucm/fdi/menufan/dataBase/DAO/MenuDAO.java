package es.ucm.fdi.menufan.dataBase.DAO;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import es.ucm.fdi.menufan.dataBase.Entities.MenuEntity;

@Dao
public interface MenuDAO {

    //Return= todos los menus
    @Query("select * from Menu")
    List<MenuEntity> getAll();

    //Return= una menu
    @Query("select * from Menu where MenuID = :menuId")
    MenuEntity findById(String menuId);

    @Insert
    void insert (MenuEntity menu);

    @Update
    void update (MenuEntity menu);

    @Delete
    void delete (MenuEntity menu);


}
