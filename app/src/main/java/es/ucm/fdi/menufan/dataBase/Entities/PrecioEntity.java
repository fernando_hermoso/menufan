package es.ucm.fdi.menufan.dataBase.Entities;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.Date;


@Entity(tableName = "Precio")
public class PrecioEntity {


    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "PrecioId")
    private int PreciID;

    @ColumnInfo(name = "gasto")
   private Double gasto;

    @ColumnInfo(name = "fecha")
    private String fecha;


    public PrecioEntity() {
    }

    public PrecioEntity(Double gasto, String fecha) {
        this.gasto = gasto;
        this.fecha = fecha;
    }

    public PrecioEntity( int preciID, Double gasto, String fecha) {
        PreciID = preciID;
        this.gasto = gasto;
        this.fecha = fecha;
    }



    public int getPreciID() {
        return PreciID;
    }

    public void setPreciID( int preciID) {
        PreciID = preciID;
    }

    public Double getGasto() {
        return gasto;
    }

    public void setGasto(Double gasto) {
        this.gasto = gasto;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
}
