package es.ucm.fdi.menufan.dataBase.DAO;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;


import es.ucm.fdi.menufan.dataBase.Entities.PrecioListaEntity;

@Dao
public interface PrecioListaDAO {


    //Return= todas precios de todas las listas
    @Query("select * from PrecioLista")
    List<PrecioListaEntity> getAll();

    //Return= todos los precios de UNA lista
    @Query("select * from PrecioLista where listaId = :listId")
    List<PrecioListaEntity> findById(String listId);

    @Insert
    void insert (PrecioListaEntity lista);

    @Update
    void update (PrecioListaEntity lista);

    @Delete
    void delete (PrecioListaEntity lista);
}
