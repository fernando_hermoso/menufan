package es.ucm.fdi.menufan;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import es.ucm.fdi.menufan.adapters.AddMenusAdapter;
import es.ucm.fdi.menufan.adapters.AddRecetaAdapter;
import es.ucm.fdi.menufan.dataBase.AppDataBase;
import es.ucm.fdi.menufan.dataBase.DAO.MenuDAO;
import es.ucm.fdi.menufan.dataBase.DAO.RecetaDAO;
import es.ucm.fdi.menufan.dataBase.Entities.MenuEntity;
import es.ucm.fdi.menufan.dataBase.Entities.RecetaEntity;

public class AddMenuLista extends AppCompatActivity implements QuantityListener{

    Button aniadirSelecion;
    Context context;

    AppDataBase dataBase;
    MenuDAO menuDAO;

    AddMenusAdapter addMenusAdapter;
    RecyclerView recyclerView;

    List<MenuEntity> listaMenus;
    List<String> recetasLocal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_menu_lista);

        init();
    }

    @Override
    protected void onResume () {
        super.onResume();
        listaMenus = menuDAO.getAll();
        addMenusAdapter.setListaProductos(listaMenus);
    }


    private void init(){

        context= this;
        recyclerView = findViewById(R.id.addMenusRecycleview);
        aniadirSelecion = findViewById(R.id.addSeleccionados);

        listaMenus = new ArrayList<>();
        recetasLocal = new ArrayList<>();

        addMenusAdapter = new AddMenusAdapter(recetasLocal,this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        recyclerView.setAdapter(addMenusAdapter);

        dataBase = AppDataBase.getInstanceDataBase(this.getApplicationContext());
        menuDAO = dataBase.menuDAO();

        // Obtengo los datos del intent
        Intent intent = getIntent();
        recetasLocal = (List<String>) intent.getSerializableExtra("listaRegistroMenus");

        aniadirSelecion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentRespuesta = new Intent();
                intentRespuesta.putExtra("listaRegistroMenus", (Serializable) recetasLocal);
                setResult(RESULT_OK,intentRespuesta);
                finish();
            }
        });

    }



    @Override
    public void arrayRegistroProductos(List<String> list) {
        recetasLocal = list;
    }
}