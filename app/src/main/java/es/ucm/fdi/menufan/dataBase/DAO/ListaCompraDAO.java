package es.ucm.fdi.menufan.dataBase.DAO;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import es.ucm.fdi.menufan.dataBase.Entities.ListaCompraEntity;
import es.ucm.fdi.menufan.dataBase.Entities.ProductoEntity;

@Dao
public interface ListaCompraDAO {

    //Return= todos las listas de compra
    @Query("select * from ListaCompra")
    List<ListaCompraEntity> getAll();

    //Return= una lista de compra
    @Query("select * from ListaCompra where NombreListaID = :listtId")
    ListaCompraEntity findById(String listtId);

    //Return= todas las listas de compras que sean favoritas
    @Query("select * from ListaCompra where favorito = 1")
    List<ListaCompraEntity>  findFavoritos();

    //Return= todas las listas de compras que sean favoritas
    @Query("select * from ListaCompra where favorito = 0")
    List<ListaCompraEntity>  findNoFavoritos();

    @Insert
    void insert (ListaCompraEntity lista);

    @Update
    void update (ListaCompraEntity lista);

    @Delete
    void delete (ListaCompraEntity lista);
}
