package es.ucm.fdi.menufan.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import es.ucm.fdi.menufan.R;
import es.ucm.fdi.menufan.dataBase.AppDataBase;
import es.ucm.fdi.menufan.dataBase.DAO.RecetaMenuDAO;
import es.ucm.fdi.menufan.dataBase.Entities.ProductoRecetasEntity;
import es.ucm.fdi.menufan.dataBase.Entities.RecetaMenuEntity;

public class MenuRecetasAdapter extends RecyclerView.Adapter<MenuRecetasAdapter.ViewHolder>{

    private List<RecetaMenuEntity> listaProductoRecetas = new ArrayList<>();
    Context context;

    public MenuRecetasAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public MenuRecetasAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_adapter_listaproducto, parent, false);
        MenuRecetasAdapter.ViewHolder holder = new MenuRecetasAdapter.ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MenuRecetasAdapter.ViewHolder holder, int position) {
        holder.asignarDatos(listaProductoRecetas.get(position));
    }

    @Override
    public int getItemCount() {
        return listaProductoRecetas.size();
    }

    public void setListaProductosRecetas(List<RecetaMenuEntity> listaProducto){
        this.listaProductoRecetas = listaProducto;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView nombreProducto;
        private AppCompatImageView eliminar;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            nombreProducto = itemView.findViewById(R.id.producto);
            eliminar = itemView.findViewById(R.id.iconoLimpiar);


        }

        public void asignarDatos(RecetaMenuEntity listaInfo) {
            nombreProducto.setText(listaInfo.getRecetaId());

            eliminar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AppDataBase dataBase= AppDataBase.getInstanceDataBase(context);
                    RecetaMenuDAO recetaMenuDAO = dataBase.recetaMenuDAO();
                    //Borro esa Receta asociada a ese menu
                    recetaMenuDAO.delete(listaInfo);

                    // Actulizo para que me muestre la lista de recetas
                    setListaProductosRecetas(recetaMenuDAO.findById(listaInfo.getMenuId()));

                }
            });

        }
    }
}
