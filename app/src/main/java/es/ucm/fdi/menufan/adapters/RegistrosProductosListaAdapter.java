package es.ucm.fdi.menufan.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import es.ucm.fdi.menufan.R;
import es.ucm.fdi.menufan.dataBase.Entities.ListacompraProductoEntity;


public class RegistrosProductosListaAdapter extends RecyclerView.Adapter<RegistrosProductosListaAdapter.ViewHolder>{

    private List<String> productosDeLista ;

    public RegistrosProductosListaAdapter(List<String> productosDeLista) {
        this.productosDeLista = productosDeLista;
    }

    public RegistrosProductosListaAdapter() {
    }

    @NonNull
    @Override
    public RegistrosProductosListaAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_registro_productos_alista, parent, false);
        RegistrosProductosListaAdapter.ViewHolder holder = new RegistrosProductosListaAdapter.ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.asignarDatos(productosDeLista.get(position));
    }

    @Override
    public int getItemCount() {
        return productosDeLista.size();
    }

    public void setListaProductos(List<String> listaProducto){
        this.productosDeLista = listaProducto;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView nombreProducto;
        private AppCompatImageView eliminar;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            nombreProducto = itemView.findViewById(R.id.producto);
            eliminar = itemView.findViewById(R.id.iconoLimpiar);
        }

        public void asignarDatos(String listaInfo) {
            nombreProducto.setText(listaInfo);
            eliminar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    productosDeLista.remove(listaInfo);
                    setListaProductos(productosDeLista);
                }
            });

        }
    }
}
