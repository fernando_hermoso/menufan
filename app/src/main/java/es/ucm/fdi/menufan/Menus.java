package es.ucm.fdi.menufan;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import es.ucm.fdi.menufan.adapters.MenuAdapter;
import es.ucm.fdi.menufan.adapters.RecetasAdapter;
import es.ucm.fdi.menufan.dataBase.AppDataBase;
import es.ucm.fdi.menufan.dataBase.DAO.MenuDAO;
import es.ucm.fdi.menufan.dataBase.Entities.MenuEntity;
import es.ucm.fdi.menufan.dataBase.Entities.RecetaEntity;

public class Menus extends AppCompatActivity implements MenuAdapter.OnNoteListener {
    BottomNavigationView navigation;
    FloatingActionButton addMenu;
    Context context;
    MenuAdapter menuAdapter;
    RecyclerView recyclerViewMenu;
    List<MenuEntity> listaMenus;

    AppDataBase dataBase;
    MenuDAO menuDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menus);
        init();
    }

    @Override
    protected void onResume () {
        super.onResume();
        listaMenus = menuDAO.getAll();
        menuAdapter.setListaMenu(listaMenus);
    }

    private void init(){
        context = this;
        menuAdapter = new MenuAdapter(this,this);
        addMenu = findViewById(R.id.botonFLotante);
        recyclerViewMenu = findViewById(R.id.menuRecycleview);
        recyclerViewMenu.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        recyclerViewMenu.setAdapter(menuAdapter);
        listaMenus = new ArrayList<>();

        //DataBase
        dataBase = AppDataBase.getInstanceDataBase(this.getApplicationContext());
        menuDAO = dataBase.menuDAO();

        addMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Agregar nuevo menu
                startActivity(new Intent(context, AddMenu.class));
            }
        });

        navigation = findViewById(R.id.menu_bottom_navigation);
        navigation.setSelectedItemId(R.id.menu_menu);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.menu_gastos:
                        startActivity(new Intent(getApplicationContext(), Gastos.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.menu_list:
                        startActivity(new Intent(getApplicationContext(), ListaCompras.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.menu_menu:
                        return true;
                    case R.id.menu_recetas:
                        startActivity(new Intent(getApplicationContext(), Recetas.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.menu_productos:
                        startActivity(new Intent(getApplicationContext(), Productos.class));
                        overridePendingTransition(0, 0);
                        return true;

                }
                return false;
            }
        });
    }

    @Override
    public void onNoteClick(int position) {
        //Crear intent para la nueva actividad
        MenuEntity a = listaMenus.get(position);
        Intent intent = new Intent(context, InfoMenu.class);
        intent.putExtra("NombreMenu", a.getNombreMenuID());
        startActivity(intent);
    }
}