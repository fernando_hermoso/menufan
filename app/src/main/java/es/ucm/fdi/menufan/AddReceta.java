package es.ucm.fdi.menufan;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import es.ucm.fdi.menufan.adapters.RegistrosProductosListaAdapter;
import es.ucm.fdi.menufan.dataBase.AppDataBase;
import es.ucm.fdi.menufan.dataBase.DAO.ProductoDAO;
import es.ucm.fdi.menufan.dataBase.DAO.ProductoRecetaDAO;
import es.ucm.fdi.menufan.dataBase.DAO.RecetaDAO;
import es.ucm.fdi.menufan.dataBase.Entities.ProductoRecetasEntity;
import es.ucm.fdi.menufan.dataBase.Entities.RecetaEntity;

public class AddReceta extends AppCompatActivity {

    private static final int REGISTER_PRODUCTOS_REQUEST = 1;

    Context context;
    EditText receta;
    EditText descripcion;
    Button guardar;
    Button floatingActionButton;

    AppDataBase dataBase;
    RecetaDAO recetaDAO;
    ProductoRecetaDAO productoRecetaDAO;

    RegistrosProductosListaAdapter registrosProductos;
    RecyclerView recyclerView;

    List<String> listaLocal;
    List<String>listaRegistroProductos;

    BottomNavigationView navigation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_receta);
        init();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REGISTER_PRODUCTOS_REQUEST){
            if(resultCode == RESULT_OK){


                listaLocal = (List<String>) data.getSerializableExtra("listaRegistroProductos");

                for(String i: listaLocal){

                    if (!buscarElemento(i)){
                        listaRegistroProductos.add(i);
                    }

                }
                registrosProductos.setListaProductos(listaRegistroProductos);
            }else if(resultCode == RESULT_CANCELED){
                Log.d("Error", "Ha ocurridor error en cargar el registro de productos");
            }

        }

    }

    private Boolean buscarElemento (String nombreProducto){

        Boolean encontrado = false;
        int j = 0;
        while (!encontrado && j < listaRegistroProductos.size()){
            if (listaRegistroProductos.get(j).equals(nombreProducto)){
                encontrado = true;
            }
            j++;
        }
        return encontrado;
    }

    private void init(){
        context = this;
        receta = findViewById(R.id.RecetaNueva);
        descripcion = findViewById(R.id.Descripcion);
        guardar = findViewById(R.id.AñadirReceta);

        dataBase = AppDataBase.getInstanceDataBase(context.getApplicationContext());
        recetaDAO = dataBase.recetaDAO();
        productoRecetaDAO= dataBase.productoRecetaDAO();

        listaLocal = new ArrayList<>();
        listaRegistroProductos = new ArrayList<>();

        registrosProductos = new RegistrosProductosListaAdapter(listaRegistroProductos);
        recyclerView = findViewById(R.id.RecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        recyclerView.setAdapter(registrosProductos);



        floatingActionButton = findViewById(R.id.botonFLotante);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AddProducto.class);
                intent.putExtra("listaRegistroProductos",(Serializable)listaLocal);
                startActivityForResult(intent,REGISTER_PRODUCTOS_REQUEST);
            }
        });

        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Comprobar si campos NOmbre y descrip estan vacios

                if(!receta.getText().toString().trim().equalsIgnoreCase("") && !receta.getText().toString().trim().equalsIgnoreCase("") ){

                    //Añadir nombre receta y descripcion en BD de REcerta
                    RecetaEntity rec = new RecetaEntity(receta.getText().toString(),descripcion.getText().toString());
                    recetaDAO.insert(rec);

                    // añadir esos productos a esa receta

                    for(int i = 0; i<listaRegistroProductos.size(); i++){

                        ProductoRecetasEntity productoRecetasEntity = new ProductoRecetasEntity( listaRegistroProductos.get(i), receta.getText().toString());
                        productoRecetaDAO.insert(productoRecetasEntity);
                    }

                    Toast.makeText(context, "Receta añadida",Toast.LENGTH_LONG).show();
                    startActivity(new Intent(context, Recetas.class));

                }else{

                    Toast.makeText(context, "Introduce un nombre y descripcion a tu REceta",Toast.LENGTH_LONG).show();
                }








            }
        });

        navigation = findViewById(R.id.menu_bottom_navigation);
        //navigation.setSelectedItemId(R.id.menu_menu);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.menu_gastos:
                        startActivity(new Intent(getApplicationContext(), Gastos.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.menu_list:
                        startActivity(new Intent(getApplicationContext(), ListaCompras.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.menu_menu:
                        startActivity(new Intent(getApplicationContext(), Menus.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.menu_recetas:
                        startActivity(new Intent(getApplicationContext(), Recetas.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.menu_productos:
                        startActivity(new Intent(getApplicationContext(), Productos.class));
                        overridePendingTransition(0, 0);
                        return true;

                }
                return false;
            }
        });
    }
}