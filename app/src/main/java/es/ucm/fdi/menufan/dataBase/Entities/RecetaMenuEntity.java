package es.ucm.fdi.menufan.dataBase.Entities;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;

@Entity(tableName = "RecetaMenu",
        primaryKeys = {"menuId", "recetaID"},
        foreignKeys = {
                @ForeignKey(
                        entity = MenuEntity.class,
                        parentColumns = "MenuID",
                        childColumns = "menuId",
                        onDelete = ForeignKey.CASCADE,
                        onUpdate = ForeignKey.CASCADE
                ),

                @ForeignKey(
                        entity = RecetaEntity.class,
                        parentColumns = "Receta",
                        childColumns = "recetaID",
                        onDelete = ForeignKey.CASCADE,
                        onUpdate = ForeignKey.CASCADE)

        })
public class RecetaMenuEntity {

    @NonNull
    @ColumnInfo(name = "menuId")
    private String menuId;


    @NonNull
    @ColumnInfo(name = "recetaID")
    private String recetaId;

    @Ignore
    public RecetaMenuEntity() {
    }

    public RecetaMenuEntity(@NonNull String menuId, @NonNull String recetaId) {
        this.menuId = menuId;
        this.recetaId = recetaId;
    }

    @NonNull
    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(@NonNull String menuId) {
        this.menuId = menuId;
    }

    @NonNull
    public String getRecetaId() {
        return recetaId;
    }

    public void setRecetaId(@NonNull String recetaId) {
        this.recetaId = recetaId;
    }
}
