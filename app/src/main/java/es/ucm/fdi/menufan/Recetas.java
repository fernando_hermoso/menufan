package es.ucm.fdi.menufan;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import es.ucm.fdi.menufan.adapters.ListaCompraAdapter;
import es.ucm.fdi.menufan.adapters.RecetasAdapter;
import es.ucm.fdi.menufan.dataBase.AppDataBase;
import es.ucm.fdi.menufan.dataBase.DAO.RecetaDAO;
import es.ucm.fdi.menufan.dataBase.Entities.ListaCompraEntity;
import es.ucm.fdi.menufan.dataBase.Entities.RecetaEntity;

public class Recetas extends AppCompatActivity implements RecetasAdapter.OnNoteListener {
    BottomNavigationView navigation;
    FloatingActionButton addReceta;
    Context context;
    RecetasAdapter recetasAdapter;
    RecyclerView recyclerViewReceta;
    List<RecetaEntity> listaRecetas;
    AppDataBase dataBase;
    RecetaDAO recetaDAO;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recetas);

        init();
    }


    @Override
    protected void onResume () {
        super.onResume();
        listaRecetas = recetaDAO.getAll();
        recetasAdapter.setListaProductos(listaRecetas);
    }


    private void init(){
        context = this;
        recetasAdapter = new RecetasAdapter(this, this);
        recyclerViewReceta = findViewById(R.id.RecetasRecycleview);
        recyclerViewReceta.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        recyclerViewReceta.setAdapter(recetasAdapter);
        addReceta = findViewById(R.id.botonFLotante);
        listaRecetas = new ArrayList<>();

        //DataBase
        dataBase = AppDataBase.getInstanceDataBase(this.getApplicationContext());
        recetaDAO = dataBase.recetaDAO();

        addReceta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Agregar Receta
                startActivity(new Intent(context, AddReceta.class));
            }
        });

        navigation = findViewById(R.id.menu_bottom_navigation);
        navigation.setSelectedItemId(R.id.menu_recetas);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.menu_gastos:
                        startActivity(new Intent(getApplicationContext(), Gastos.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.menu_list:
                        startActivity(new Intent(getApplicationContext(), ListaCompras.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.menu_menu:
                        startActivity(new Intent(getApplicationContext(), Menus.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.menu_recetas:
                        return true;
                    case R.id.menu_productos:
                        startActivity(new Intent(getApplicationContext(), Productos.class));
                        overridePendingTransition(0, 0);
                        return true;

                }
                return false;
            }
        });
    }

    @Override
    public void onNoteClick(int position) {
        //Crear intent para la nueva actividad
        RecetaEntity a = listaRecetas.get(position);
        Intent intent = new Intent(context, InfoRecetas.class);
        intent.putExtra("NombreReceta", a.getNombreRecetaID());
        intent.putExtra("DescripcionReceta", a.getDescripcion());
        startActivity(intent);
    }
}