package es.ucm.fdi.menufan.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import es.ucm.fdi.menufan.QuantityListener;
import es.ucm.fdi.menufan.R;
import es.ucm.fdi.menufan.dataBase.Entities.ProductoEntity;
import es.ucm.fdi.menufan.dataBase.Entities.RecetaEntity;


public class AddRecetaAdapter extends RecyclerView.Adapter<AddRecetaAdapter.ViewHolder>{

    private List<RecetaEntity> lista ;
    private List<String> registrarRecetas;
    QuantityListener quantityListener;


    public AddRecetaAdapter(List<String> registrarRecetas, QuantityListener quantityListener) {
        this.registrarRecetas = registrarRecetas;
        this.quantityListener = quantityListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_adapter_add_productos, parent, false);
        AddRecetaAdapter.ViewHolder holder = new AddRecetaAdapter.ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.asignarDatos(lista.get(position));
    }

    @Override
    public int getItemCount() {
       return lista.size();
    }

    public void setListaProductos(List<RecetaEntity> listaProducto){
        this.lista = listaProducto;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView nombre;
        CheckBox marcada;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            nombre = itemView.findViewById(R.id.producto);
            marcada =  itemView.findViewById(R.id.checkBox);
        }

        public void asignarDatos(RecetaEntity listaInfo) {

            nombre.setText(listaInfo.getNombreRecetaID());
            marcada.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (marcada.isChecked()){
                        registrarRecetas.add(listaInfo.getNombreRecetaID());

                    }else {
                        registrarRecetas.remove(listaInfo.getNombreRecetaID());

                    }
                    quantityListener.arrayRegistroProductos(registrarRecetas);
                }
            });
        }
    }
}
