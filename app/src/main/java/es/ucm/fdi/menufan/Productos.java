package es.ucm.fdi.menufan;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import es.ucm.fdi.menufan.adapters.ListaCompraAdapter;
import es.ucm.fdi.menufan.adapters.ProductoAdapter;
import es.ucm.fdi.menufan.adapters.RecetasAdapter;
import es.ucm.fdi.menufan.dataBase.AppDataBase;
import es.ucm.fdi.menufan.dataBase.DAO.ListaCompraDAO;
import es.ucm.fdi.menufan.dataBase.DAO.ProductoDAO;
import es.ucm.fdi.menufan.dataBase.Entities.ListaCompraEntity;
import es.ucm.fdi.menufan.dataBase.Entities.MenuEntity;
import es.ucm.fdi.menufan.dataBase.Entities.ProductoEntity;

public class Productos extends AppCompatActivity implements ProductoAdapter.OnNoteListener{
    BottomNavigationView navigation;
   // FloatingActionButton addProducto;
    Context context;
    ProductoAdapter productoAdapter;
    RecyclerView recyclerViewProducto;
    List<ProductoEntity> listaProducto;
    AppDataBase dataBase;
    ProductoDAO productoDAO;

    public static List<ProductoID> listaIDs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_productos);
        init();
    }

    @Override
    protected void onResume () {
        super.onResume();
        listaProducto = productoDAO.getAll();
        productoAdapter.setListaProductos(listaProducto);
    }


    private void init(){

        context = this;
        productoAdapter = new ProductoAdapter(context,this);
     //   addProducto = findViewById(R.id.botonFLotante);
        recyclerViewProducto = findViewById(R.id.productosRecycleview);
        recyclerViewProducto.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        recyclerViewProducto.setAdapter(productoAdapter);
        listaProducto = new ArrayList<>();

        //DataBase
        dataBase = AppDataBase.getInstanceDataBase(this.getApplicationContext());
        productoDAO = dataBase.productoDAO();

        /*
        addProducto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Agregar lista Compra nueva
                startActivity(new Intent(context, AddProducto.class));
            }
        });*/
        creaListaProducto();

        navigation = findViewById(R.id.menu_bottom_navigation);
        navigation.setSelectedItemId(R.id.menu_productos);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.menu_gastos:
                        startActivity(new Intent(getApplicationContext(), Gastos.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.menu_list:
                        startActivity(new Intent(getApplicationContext(), ListaCompras.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.menu_menu:
                        startActivity(new Intent(getApplicationContext(), Menus.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.menu_recetas:
                        startActivity(new Intent(getApplicationContext(), Recetas.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.menu_productos:
                        return true;

                }
                return false;
            }
        });

    }

    @Override
    public void onNoteClick(int position) {
        //Crear intent para la nueva actividad
        ProductoEntity a = listaProducto.get(position);
        Intent intent = new Intent(context, InfoProducto.class);
        intent.putExtra("NombreProducto", a.getNombreProductoID());
        startActivity(intent);
    }

    private void creaListaProducto(){

        listaIDs = new ArrayList<>();
        InputStream is = getResources().openRawResource(R.raw.productosid);
        BufferedReader bf = new BufferedReader(new InputStreamReader(is));
        String linea = "";
        try{
            while((linea = bf.readLine())!= null){
                String[] t = linea.split(";");

                ProductoID prod = new ProductoID();
                prod.setNombre(t[0]);
                prod.setId(t[1]);
                listaIDs.add(prod);

            }
        }catch(IOException e){
            Log.wtf("Error lectura fichero IDs", "error en la linea " + linea, e);
            e.printStackTrace();
        }
    }

}