package es.ucm.fdi.menufan.dataBase;


import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import es.ucm.fdi.menufan.dataBase.DAO.ListaCompraDAO;
import es.ucm.fdi.menufan.dataBase.DAO.ListacompraProductoDAO;
import es.ucm.fdi.menufan.dataBase.DAO.MenuDAO;
import es.ucm.fdi.menufan.dataBase.DAO.PrecioDAO;
import es.ucm.fdi.menufan.dataBase.DAO.PrecioListaDAO;
import es.ucm.fdi.menufan.dataBase.DAO.ProductoDAO;
import es.ucm.fdi.menufan.dataBase.DAO.ProductoRecetaDAO;
import es.ucm.fdi.menufan.dataBase.DAO.RecetaDAO;
import es.ucm.fdi.menufan.dataBase.DAO.RecetaMenuDAO;
import es.ucm.fdi.menufan.dataBase.Entities.ListaCompraEntity;
import es.ucm.fdi.menufan.dataBase.Entities.ListacompraProductoEntity;
import es.ucm.fdi.menufan.dataBase.Entities.MenuEntity;
import es.ucm.fdi.menufan.dataBase.Entities.PrecioEntity;
import es.ucm.fdi.menufan.dataBase.Entities.PrecioListaEntity;
import es.ucm.fdi.menufan.dataBase.Entities.ProductoEntity;
import es.ucm.fdi.menufan.dataBase.Entities.ProductoRecetasEntity;
import es.ucm.fdi.menufan.dataBase.Entities.RecetaEntity;
import es.ucm.fdi.menufan.dataBase.Entities.RecetaMenuEntity;


@Database(entities = {
        ListaCompraEntity.class,
        ListacompraProductoEntity.class,
        MenuEntity.class,
        PrecioEntity.class,
        PrecioListaEntity.class,
        ProductoEntity.class,
        ProductoRecetasEntity.class,
        RecetaEntity.class,
        RecetaMenuEntity.class

},version = 4)
public abstract class AppDataBase extends RoomDatabase {

    private static  AppDataBase INSTANCE;

    public abstract ListaCompraDAO listaCompraDao();
    public abstract ListacompraProductoDAO listacompraProductoDAO();
    public abstract MenuDAO menuDAO();
    public abstract PrecioDAO precioDAO();
    public abstract PrecioListaDAO precioListaDAO();
    public abstract ProductoDAO productoDAO();
    public abstract ProductoRecetaDAO productoRecetaDAO();
    public abstract RecetaDAO recetaDAO();
    public abstract RecetaMenuDAO recetaMenuDAO();


    public static AppDataBase getInstanceDataBase(Context context){
        if (INSTANCE == null){

            synchronized (AppDataBase.class){

                if (INSTANCE == null){
                    //Create database here
                    INSTANCE = Room.databaseBuilder(context,AppDataBase.class,"MenuFanDataBase.db")
                            .allowMainThreadQueries()
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
    }



}
