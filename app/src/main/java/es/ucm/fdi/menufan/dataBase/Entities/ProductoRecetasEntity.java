package es.ucm.fdi.menufan.dataBase.Entities;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;

@Entity(tableName = "ProductoRecetas",
        primaryKeys = {"productoID", "recetaID"},
        foreignKeys = {
                @ForeignKey(
                        entity = ProductoEntity.class,
                        parentColumns = "producto",
                        childColumns = "productoID",
                        onDelete = ForeignKey.CASCADE,
                        onUpdate = ForeignKey.CASCADE
                ),

                @ForeignKey(
                        entity = RecetaEntity.class,
                        parentColumns = "Receta",
                        childColumns = "recetaID",
                        onDelete = ForeignKey.CASCADE,
                        onUpdate = ForeignKey.CASCADE)

        })
public class ProductoRecetasEntity {


    @NonNull
    @ColumnInfo(name = "productoID")
    private String productoId;


    @NonNull
    @ColumnInfo(name = "recetaID")
    private String recetaId;

    @Ignore
    public ProductoRecetasEntity() {
    }

    public ProductoRecetasEntity(@NonNull String productoId, @NonNull String recetaId) {
        this.productoId = productoId;
        this.recetaId = recetaId;
    }

    @NonNull
    public String getProductoId() {
        return productoId;
    }

    public void setProductoId(@NonNull String productoId) {
        this.productoId = productoId;
    }

    @NonNull
    public String getRecetaId() {
        return recetaId;
    }

    public void setRecetaId(@NonNull String recetaId) {
        this.recetaId = recetaId;
    }
}
