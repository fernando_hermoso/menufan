package es.ucm.fdi.menufan;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import es.ucm.fdi.menufan.adapters.RegistrosProductosListaAdapter;
import es.ucm.fdi.menufan.dataBase.AppDataBase;
import es.ucm.fdi.menufan.dataBase.DAO.ListaCompraDAO;
import es.ucm.fdi.menufan.dataBase.DAO.ListacompraProductoDAO;
import es.ucm.fdi.menufan.dataBase.DAO.ProductoRecetaDAO;
import es.ucm.fdi.menufan.dataBase.DAO.RecetaMenuDAO;
import es.ucm.fdi.menufan.dataBase.Entities.ListaCompraEntity;
import es.ucm.fdi.menufan.dataBase.Entities.ListacompraProductoEntity;
import es.ucm.fdi.menufan.dataBase.Entities.ProductoRecetasEntity;
import es.ucm.fdi.menufan.dataBase.Entities.RecetaMenuEntity;

public class AddListaCompra extends AppCompatActivity {

    /*
    * Esta clase se encarga de crear una nueva lsita de la compra en la que podras meter sus productos de 3 formas diferentes(Productos, Recetas, Menu)
    * */
    private static final int REGISTER_PRODUCTOS_REQUEST = 1;
    private static final int REGISTER_RECETAS_REQUEST = 2;
    private static final int REGISTER_MENU_REQUEST = 3;
    TextView nombreLista;
    Button addProducto;
    Button addReceta;
    Button addMenu;
    Button guardar;

    AppDataBase dataBase;
    ListaCompraDAO listaCompraDAO;
    ListacompraProductoDAO listacompraProductoDAO;
    ProductoRecetaDAO productoRecetaDAO;
    RecetaMenuDAO recetaMenuDAO;
    BottomNavigationView navigation;

    Context context;

    List<String>listaRegistroProductos;
    List<String> listaLocal;


    RegistrosProductosListaAdapter registrosProductosListaAdapter;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_lista_compra);

        Init();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REGISTER_PRODUCTOS_REQUEST){
            if(resultCode == RESULT_OK){


                listaLocal = (List<String>) data.getSerializableExtra("listaRegistroProductos");

                for(String i: listaLocal){

                    if (!buscarElemento(i)){
                        listaRegistroProductos.add(i);
                    }

                }
                listaLocal.clear();
                registrosProductosListaAdapter.setListaProductos(listaRegistroProductos);
            }else if(resultCode == RESULT_CANCELED){
                Log.d("Error", "Ha ocurridor error en cargar el registro de productos");
            }

        }

        if (requestCode == REGISTER_RECETAS_REQUEST){
            if(resultCode == RESULT_OK){
                listaLocal = (List<String>) data.getSerializableExtra("listaRegistroRecetas");
                for(String i: listaLocal){



                    List<ProductoRecetasEntity> aux = new ArrayList<>();

                    aux = productoRecetaDAO.findById(i);

                    for(int j = 0; j < aux.size(); j++){

                        if (!buscarElemento(aux.get(j).getProductoId())){
                            listaRegistroProductos.add(aux.get(j).getProductoId());
                        }
                    }



                }
                listaLocal.clear();
                registrosProductosListaAdapter.setListaProductos(listaRegistroProductos);
            }


        }

        if(requestCode == REGISTER_MENU_REQUEST){
            if(resultCode == RESULT_OK){

                listaLocal = (List<String>) data.getSerializableExtra("listaRegistroMenus");
                for(String i: listaLocal){

                    // meterme para cada ID Menu extarer sus Recetas

                    List<RecetaMenuEntity> aux = new ArrayList<>();

                    aux = recetaMenuDAO.findById(i);// LAs recetas asociadas a un menu

                    for(int j = 0; j < aux.size(); j++){
                        //buscamos los productos asociados a esa Receta
                        List<ProductoRecetasEntity> aux2 = new ArrayList<>();
                        aux2 = productoRecetaDAO.findById(aux.get(j).getRecetaId());

                        for(int l = 0; l < aux2.size(); l++){

                            if (!buscarElemento(aux2.get(l).getProductoId())){
                                listaRegistroProductos.add(aux2.get(l).getProductoId());
                            }
                        }

                    }

                }
                listaLocal.clear();
                registrosProductosListaAdapter.setListaProductos(listaRegistroProductos);
            }
        }

    }

    private Boolean buscarElemento (String nombreProducto){

        Boolean encontrado = false;
        int j = 0;
        while (!encontrado && j < listaRegistroProductos.size()){
            if (listaRegistroProductos.get(j).equals(nombreProducto)){
                encontrado = true;
            }
            j++;
        }
        return encontrado;
    }

    private void Init() {
        nombreLista = findViewById(R.id.nombreListaCompraNueva);
        guardar = findViewById(R.id.AniadirALista);
        addProducto = findViewById(R.id.aniadirProductoLista);
        addReceta = findViewById(R.id.aniadirRecetaLista);
        addMenu = findViewById(R.id.aniadirMenuLista);

        listaRegistroProductos = new ArrayList<>();
        listaLocal = new ArrayList<>();

        registrosProductosListaAdapter = new RegistrosProductosListaAdapter(listaRegistroProductos);
        recyclerView = findViewById(R.id.RecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        recyclerView.setAdapter(registrosProductosListaAdapter);

        dataBase = AppDataBase.getInstanceDataBase(this.getApplicationContext());
        listaCompraDAO = dataBase.listaCompraDao();
        listacompraProductoDAO = dataBase.listacompraProductoDAO();
        productoRecetaDAO = dataBase.productoRecetaDAO();
        recetaMenuDAO = dataBase.recetaMenuDAO();

        context = this;

        addProducto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AddProducto.class);
                intent.putExtra("listaRegistroProductos",(Serializable)listaLocal);
               // intent.putExtra("AddListaCompra",nombreLista.getText().toString());
                startActivityForResult(intent,REGISTER_PRODUCTOS_REQUEST);
            }
        });

        addReceta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AddRecetasLista.class);
                intent.putExtra("listaRegistroRecetas",(Serializable)listaLocal);
                // intent.putExtra("AddListaCompra",nombreLista.getText().toString());
                startActivityForResult(intent,REGISTER_RECETAS_REQUEST);
            }
        });

        addMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AddMenuLista.class);
                intent.putExtra("listaRegistroMenus",(Serializable)listaLocal);
                // intent.putExtra("AddListaCompra",nombreLista.getText().toString());
                startActivityForResult(intent,REGISTER_MENU_REQUEST);
            }
        });
        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Comprobar di el nombre de lista esta vacio
                if(!nombreLista.getText().toString().trim().equalsIgnoreCase("")){
                    // Primero Inseretar el nombre de la lista en la entida lista
                    ListaCompraEntity list = new ListaCompraEntity(nombreLista.getText().toString(),false);
                    listaCompraDAO.insert(list);
                  //  Toast.makeText(context, "Lista añadida",Toast.LENGTH_LONG).show();
                  //  startActivity(new Intent(context, ListaCompras.class));

                    // 2º Insertar nombre lista y productos en listaProductos

                    for (int i = 0; i<listaRegistroProductos.size(); i++ ){
                        ListacompraProductoEntity listacompraProductoEntity = new ListacompraProductoEntity(nombreLista.getText().toString(), listaRegistroProductos.get(i),1);
                        listacompraProductoDAO.insert(listacompraProductoEntity);
                    }

                    Intent intent = new Intent(context, ListaCompras.class);
                    startActivity(intent);
                }else{
                    Toast.makeText(context, "Introduce un nombre para tu lista",Toast.LENGTH_LONG).show();
                }

            }
        });


        navigation = findViewById(R.id.menu_bottom_navigation);
       // navigation.setSelectedItemId(R.id.menu_menu);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.menu_gastos:
                        startActivity(new Intent(getApplicationContext(), Gastos.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.menu_list:
                        startActivity(new Intent(getApplicationContext(), ListaCompras.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.menu_menu:
                        startActivity(new Intent(getApplicationContext(), Menus.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.menu_recetas:
                        startActivity(new Intent(getApplicationContext(), Recetas.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.menu_productos:
                        startActivity(new Intent(getApplicationContext(), Productos.class));
                        overridePendingTransition(0, 0);
                        return true;

                }
                return false;
            }
        });

    }
}