package es.ucm.fdi.menufan;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import es.ucm.fdi.menufan.adapters.AddProductoAdapter;
import es.ucm.fdi.menufan.adapters.ProductoAdapter;
import es.ucm.fdi.menufan.dataBase.AppDataBase;
import es.ucm.fdi.menufan.dataBase.DAO.ListacompraProductoDAO;
import es.ucm.fdi.menufan.dataBase.DAO.ProductoDAO;
import es.ucm.fdi.menufan.dataBase.Entities.ListaCompraEntity;
import es.ucm.fdi.menufan.dataBase.Entities.ListacompraProductoEntity;
import es.ucm.fdi.menufan.dataBase.Entities.ProductoEntity;

public class AddProducto extends AppCompatActivity implements QuantityListener{

    Context context;
    TextView producto;
    Button guardar;
    Button guardarSeleccionados;
    AppDataBase dataBase;
    ProductoDAO productoDAO;
    ListacompraProductoDAO listacompraProductoDAO;
    RecyclerView recyclerView;
    AddProductoAdapter addProductoAdapter;
    List<ProductoEntity> listaProducto;
    List<String> registroProductos;
 //   String nombreLista;

    List<String> productosLocal;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_producto);

        Init();

    }

    @Override
    protected void onResume () {
        super.onResume();
        listaProducto = productoDAO.getAll();
        addProductoAdapter.setListaProductos(listaProducto);
    }

    private void Init() {
        producto = findViewById(R.id.productoNuevo);
        guardar = findViewById(R.id.AñadirProducto);
        guardarSeleccionados = findViewById(R.id.addSeleccionados);

        listaProducto = new ArrayList<>();
        registroProductos = new ArrayList<>();
        productosLocal = new ArrayList<>();

        context = this;
        addProductoAdapter = new AddProductoAdapter(productosLocal,this);

        recyclerView = findViewById(R.id.addProductosRecycleview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        recyclerView.setAdapter(addProductoAdapter);


        dataBase = AppDataBase.getInstanceDataBase(this.getApplicationContext());
        productoDAO = dataBase.productoDAO();
        listacompraProductoDAO = dataBase.listacompraProductoDAO();

        //DataBase
        context = this;
        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Comprobar di el nombre del producto esta vacio
                if(!producto.getText().toString().trim().equalsIgnoreCase("")){
                    // 1º Insertamos el producto en la entidad producto
                    ProductoEntity list = new ProductoEntity(producto.getText().toString().toUpperCase());
                    productoDAO.insert(list);
                    // 2º insertamos el nombre de la lista y del producto en la relacion
                 //   ListacompraProductoEntity listacompraProductoEntity = new ListacompraProductoEntity(nombreLista,producto.getText().toString(),1);
                //    listacompraProductoDAO.insert(listacompraProductoEntity);

                    listaProducto = productoDAO.getAll();
                    addProductoAdapter.setListaProductos(listaProducto);
                    producto.setText("");



                }else{
                    Toast.makeText(context, "Introduce un nombre para tu producto",Toast.LENGTH_LONG).show();
                }




            }
        });


        guardarSeleccionados.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Recorrer recyclerView
                //Los marcado los meto en la  lista que tengo que devovler




                Intent intentRespuesta = new Intent();
                intentRespuesta.putExtra("listaRegistroProductos", (Serializable) productosLocal);
                setResult(RESULT_OK,intentRespuesta);
                finish();
            }
        });
        // Obtengo los datos del intent
        Intent intent = getIntent();
        productosLocal = (List<String>) intent.getSerializableExtra("listaRegistroProductos");

        //Obtener nombre de la Lista para meterlo en listaProductoEntity
       // nombreLista = intent.getStringExtra("AddListaCompra");
    }


    @Override
    public void arrayRegistroProductos(List<String> list) {
        productosLocal = list;
    }
}