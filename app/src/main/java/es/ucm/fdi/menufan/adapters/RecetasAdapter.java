package es.ucm.fdi.menufan.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import es.ucm.fdi.menufan.R;
import es.ucm.fdi.menufan.dataBase.AppDataBase;
import es.ucm.fdi.menufan.dataBase.DAO.RecetaDAO;
import es.ucm.fdi.menufan.dataBase.Entities.ListaCompraEntity;
import es.ucm.fdi.menufan.dataBase.Entities.ProductoEntity;
import es.ucm.fdi.menufan.dataBase.Entities.RecetaEntity;

public class RecetasAdapter extends RecyclerView.Adapter<RecetasAdapter.ViewHolder> {

    private List<RecetaEntity> listaRecetas = new ArrayList<>();
    private OnNoteListener mOnNoteListener;
    private Context context;

    public RecetasAdapter(OnNoteListener onNoteListener, Context context) {
        this.mOnNoteListener = onNoteListener;
        this.context = context;
    }


    @NonNull
    @Override
    public RecetasAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_adapter_receta, parent, false);
        ViewHolder holder = new RecetasAdapter.ViewHolder(view, mOnNoteListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecetasAdapter.ViewHolder holder, int position) {
        holder.asignarDatos(listaRecetas.get(position));
    }

    @Override
    public int getItemCount() {
        return listaRecetas.size();
    }


    public void setListaProductos(List<RecetaEntity> listaRecetas){
        this.listaRecetas = listaRecetas;
        notifyDataSetChanged();
    }



    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView nombreReceta;
        private AppCompatImageView eliminar;
        private Button AniadirALista;
        OnNoteListener onNoteListener;





        public ViewHolder(@NonNull View itemVie ,OnNoteListener onNoteListener)  {
            super(itemVie);
            nombreReceta = itemView.findViewById(R.id.nombreReceta_card);
            eliminar = itemView.findViewById(R.id.iconoBorrar);
            //AniadirALista = itemView.findViewById(R.id.AniadirALista);
            this.onNoteListener = onNoteListener;
            itemView.setOnClickListener(this);


        }

        public void asignarDatos(RecetaEntity listaInfo) {
            nombreReceta.setText(listaInfo.getNombreRecetaID());

            eliminar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AppDataBase dataBase = AppDataBase.getInstanceDataBase(context);
                    RecetaDAO recetaDAO= dataBase.recetaDAO();

                    // Obtengo la lista por el id
                    String nombre = listaInfo.getNombreRecetaID();
                    RecetaEntity listaCompraEntity = recetaDAO.findById(nombre);

                    recetaDAO.delete(listaCompraEntity);

                    // Actualizo la lista
                    listaRecetas = recetaDAO.getAll();
                    setListaProductos(listaRecetas);
                }
            });
        }

        @Override
        public void onClick(View v) {
            onNoteListener.onNoteClick(getAdapterPosition());
        }
    }

    public interface  OnNoteListener{
        void onNoteClick (int position);
    }
}
