package es.ucm.fdi.menufan.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import es.ucm.fdi.menufan.R;
import es.ucm.fdi.menufan.dataBase.AppDataBase;
import es.ucm.fdi.menufan.dataBase.DAO.ProductoDAO;
import es.ucm.fdi.menufan.dataBase.DAO.ProductoRecetaDAO;
import es.ucm.fdi.menufan.dataBase.Entities.ListacompraProductoEntity;
import es.ucm.fdi.menufan.dataBase.Entities.ProductoRecetasEntity;

public class RecetasProductosAdapter  extends RecyclerView.Adapter<RecetasProductosAdapter.ViewHolder>{

    private List<ProductoRecetasEntity> listaProductoRecetas = new ArrayList<>();
    private String nombreLista;
    private Context context;

    public RecetasProductosAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_adapter_listaproducto, parent, false);
        RecetasProductosAdapter.ViewHolder holder = new RecetasProductosAdapter.ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.asignarDatos(listaProductoRecetas.get(position));
    }

    @Override
    public int getItemCount() {
       return listaProductoRecetas.size();
    }

    public void setListaProductosRecetas(List<ProductoRecetasEntity> listaProducto){
        this.listaProductoRecetas = listaProducto;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView nombreProducto;
        private AppCompatImageView eliminar;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            nombreProducto = itemView.findViewById(R.id.producto);
            eliminar = itemView.findViewById(R.id.iconoLimpiar);


        }

        public void asignarDatos(ProductoRecetasEntity listaInfo) {
            nombreProducto.setText(listaInfo.getProductoId());
            eliminar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {



                    AppDataBase dataBase = AppDataBase.getInstanceDataBase(context);
                    ProductoRecetaDAO productoRecetaDAO = dataBase.productoRecetaDAO();
                    //Borro el producto de la lista
                    productoRecetaDAO.delete(listaInfo);
                    // Actulizo para que me muestre la lista de Productos asociados a la receta
                    setListaProductosRecetas(productoRecetaDAO.findById(listaInfo.getRecetaId()));


/*
                    String producto = listaInfo.getProductoId();
                    String receta = listaInfo.getRecetaId();
                    List<ProductoRecetasEntity> productoRecetasEntity = productoRecetaDAO.findById(receta);

                    for(ProductoRecetasEntity i : productoRecetasEntity){

                        if(i.getProductoId().equals(producto) && i.getRecetaId().equals(receta)){
                            productoRecetaDAO.delete(i);
                            listaProductoRecetas = productoRecetaDAO.findById(receta);
                            setListaProductosRecetas(listaProductoRecetas);
                        }
                    }*/


                }
            });


        }
    }
}
