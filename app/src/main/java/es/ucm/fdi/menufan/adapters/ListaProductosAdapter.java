package es.ucm.fdi.menufan.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import es.ucm.fdi.menufan.R;
import es.ucm.fdi.menufan.dataBase.AppDataBase;
import es.ucm.fdi.menufan.dataBase.DAO.ListacompraProductoDAO;
import es.ucm.fdi.menufan.dataBase.Entities.ListacompraProductoEntity;
import es.ucm.fdi.menufan.dataBase.Entities.ProductoEntity;
import es.ucm.fdi.menufan.dataBase.Entities.ProductoRecetasEntity;

public class ListaProductosAdapter  extends RecyclerView.Adapter<ListaProductosAdapter.ViewHolder> {

    private List<ListacompraProductoEntity> listaProducto = new ArrayList<>();

    private String nombreLista;
    private Context context;

    public ListaProductosAdapter( Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public ListaProductosAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_adapter_listaproducto, parent, false);
        ListaProductosAdapter.ViewHolder holder = new ListaProductosAdapter.ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ListaProductosAdapter.ViewHolder holder, int position) {
        holder.asignarDatos(listaProducto.get(position));
    }

    @Override
    public int getItemCount() {
        return listaProducto.size();
    }


    public void setListaProductos(List<ListacompraProductoEntity> listaProducto){
        this.listaProducto = listaProducto;
        notifyDataSetChanged();
    }





    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView nombreProducto;
        private AppCompatImageView eliminar;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            nombreProducto = itemView.findViewById(R.id.producto);
            eliminar = itemView.findViewById(R.id.iconoLimpiar);


        }

        public void asignarDatos(ListacompraProductoEntity listaInfo) {
            nombreProducto.setText(listaInfo.getNombreProducto());
            eliminar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AppDataBase dataBase = AppDataBase.getInstanceDataBase(context);
                    ListacompraProductoDAO listacompraProductoDAO = dataBase.listacompraProductoDAO();

                    String producto = listaInfo.getNombreProducto();
                    String compra = listaInfo.getNombreListaId();

                    List<ListacompraProductoEntity> listCompra = listacompraProductoDAO.findById(compra);

                    for (ListacompraProductoEntity i: listCompra){
                        if(i.getNombreListaId().equals(compra) && i.getNombreProducto().equals(producto)){
                            listacompraProductoDAO.delete(i);
                            listaProducto = listacompraProductoDAO.findById(compra);
                            setListaProductos(listaProducto);
                        }
                    }
                }
            });


        }
    }
}
