package es.ucm.fdi.menufan.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import es.ucm.fdi.menufan.R;
import es.ucm.fdi.menufan.dataBase.AppDataBase;
import es.ucm.fdi.menufan.dataBase.DAO.ProductoDAO;
import es.ucm.fdi.menufan.dataBase.Entities.ListaCompraEntity;
import es.ucm.fdi.menufan.dataBase.Entities.ProductoEntity;

public class ProductoAdapter extends RecyclerView.Adapter<ProductoAdapter.ViewHolder> {

    private List<ProductoEntity> listaProducto = new ArrayList<>();
    private  OnNoteListener mOnNoteListener;
    private Context context;

    public ProductoAdapter(Context context,  OnNoteListener mOnNoteListener) {
        this.context = context;
        this.mOnNoteListener = mOnNoteListener;
    }

    @NonNull
    @Override
    public ProductoAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_adapter_producto, parent, false);
        ViewHolder holder = new ProductoAdapter.ViewHolder(view,mOnNoteListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ProductoAdapter.ViewHolder holder, int position) {
        holder.asignarDatos(listaProducto.get(position));
    }

    @Override
    public int getItemCount() {
        return listaProducto.size();
    }


    public void setListaProductos(List<ProductoEntity> listaProducto){
        this.listaProducto = listaProducto;
        notifyDataSetChanged();
    }



    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView nombreProducto;
        private AppCompatImageView eliminar;
        OnNoteListener onNoteListener;


        public ViewHolder(@NonNull View itemView,  OnNoteListener onNoteListener) {
            super(itemView);
            nombreProducto = itemView.findViewById(R.id.nombreProducto_card);
            eliminar = itemView.findViewById(R.id.iconoBorrar);

            this.onNoteListener = onNoteListener;
            itemView.setOnClickListener(this);






        }

        public void asignarDatos(ProductoEntity listaInfo) {
            nombreProducto.setText(listaInfo.getNombreProductoID());
            eliminar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AppDataBase dataBase = AppDataBase.getInstanceDataBase(context);
                    ProductoDAO productoDAO = dataBase.productoDAO();

                    // obtengo el id del producto
                    String produto = listaInfo.getNombreProductoID();
                    ProductoEntity productoEntity = productoDAO.findById(produto);

                    //borro
                    productoDAO.delete(productoEntity);

                    //Actializamos

                    listaProducto = productoDAO.getAll();
                    setListaProductos(listaProducto);
                }
            });
        }

        @Override
        public void onClick(View v) {
            onNoteListener.onNoteClick(getAdapterPosition());
        }
    }


    public interface  OnNoteListener{
        void onNoteClick (int position);
    }
}
