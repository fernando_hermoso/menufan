package es.ucm.fdi.menufan.dataBase.Entities;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "ListaCompra")
public class ListaCompraEntity {


    public ListaCompraEntity() {}


    @PrimaryKey
    @NonNull
    @ColumnInfo(name ="NombreListaID")
    private String NombreListaID;

    @ColumnInfo(name ="favorito")
    private Boolean favorito;



    public ListaCompraEntity(@NonNull String nombreListaID, Boolean favorito) {
        this.NombreListaID = nombreListaID;
        this.favorito = favorito;
    }

    @NonNull
    public String getNombreListaID() {
        return NombreListaID;
    }

    public Boolean getFavorito() {
        return favorito;
    }

    public void setFavorito(Boolean favorito) {
        this.favorito = favorito;
    }

    public void setNombreListaID(@NonNull String nombreListaID) {
        NombreListaID = nombreListaID;
    }
}
