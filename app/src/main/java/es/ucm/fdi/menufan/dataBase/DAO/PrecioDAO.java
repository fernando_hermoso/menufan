package es.ucm.fdi.menufan.dataBase.DAO;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import es.ucm.fdi.menufan.dataBase.Entities.PrecioEntity;

@Dao
public interface PrecioDAO {

    //Return= todos los gastos
    @Query("select * from Precio")
    List<PrecioEntity> getAll();

    //Return= una gasto
    @Query("select * from Precio where PrecioId = :gastoId")
    PrecioEntity findById(Integer gastoId);

    @Insert
    void insert (PrecioEntity gasto);

    @Update
    void update (PrecioEntity gasto);

    @Delete
    void delete (PrecioEntity gasto);
}
