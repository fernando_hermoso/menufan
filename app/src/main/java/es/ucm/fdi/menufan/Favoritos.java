package es.ucm.fdi.menufan;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

import es.ucm.fdi.menufan.adapters.ListaCompraAdapter;
import es.ucm.fdi.menufan.dataBase.AppDataBase;
import es.ucm.fdi.menufan.dataBase.DAO.ListaCompraDAO;
import es.ucm.fdi.menufan.dataBase.Entities.ListaCompraEntity;

public class Favoritos extends AppCompatActivity {
    BottomNavigationView navigation;
    BottomNavigationView tabs;

    Context context;
    ListaCompraAdapter listaCompraAdapter;
    RecyclerView recyclerViewLista;
    List<ListaCompraEntity> listaCompra;
    AppDataBase dataBase;
    ListaCompraDAO listaCompraDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favoritos);
        init();
    }

    @Override
    protected void onResume () {
        super.onResume();
       // listaCompra = listaCompraDAO.getAll();
        listaCompra = listaCompraDAO.findFavoritos();
        //obtener todas las listas de compras con favoritos a true
        listaCompraAdapter.setListaCompra(listaCompra);


    }



    private void init(){

        context = this;
        //Crear Adaptador
        listaCompraAdapter = new ListaCompraAdapter( this);

        recyclerViewLista = findViewById(R.id.favoritosRecycleview);
        recyclerViewLista.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        recyclerViewLista.setAdapter(listaCompraAdapter);
        listaCompra = new ArrayList<>();

        //DataBase
        dataBase = AppDataBase.getInstanceDataBase(this.getApplicationContext());
        listaCompraDAO = dataBase.listaCompraDao();

        navigation = findViewById(R.id.menu_bottom_navigation);
        navigation.setSelectedItemId(R.id.menu_list);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.menu_gastos:
                        startActivity(new Intent(getApplicationContext(), Gastos.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.menu_list:
                        return true;
                    case R.id.menu_menu:
                        startActivity(new Intent(getApplicationContext(), Menus.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.menu_recetas:
                        startActivity(new Intent(getApplicationContext(), Recetas.class));
                        overridePendingTransition(0, 0);
                        return true;

                }
                return false;
            }
        });


        tabs = findViewById(R.id.tabs_upper_list);
        tabs.setSelectedItemId(R.id.tab_favoritos);

        tabs.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.tabLista:
                        startActivity(new Intent(getApplicationContext(), ListaCompras.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.tab_favoritos:
                        return true;


                }
                return false;
            }
        });



    }
}