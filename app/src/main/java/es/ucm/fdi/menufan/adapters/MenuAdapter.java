package es.ucm.fdi.menufan.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import es.ucm.fdi.menufan.R;
import es.ucm.fdi.menufan.dataBase.AppDataBase;
import es.ucm.fdi.menufan.dataBase.DAO.MenuDAO;
import es.ucm.fdi.menufan.dataBase.Entities.MenuEntity;
import es.ucm.fdi.menufan.dataBase.Entities.ProductoEntity;

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.ViewHolder> {

    private List<MenuEntity> listaMenus = new ArrayList<>();
    private OnNoteListener mOnNoteListener;
    private Context context;

    public MenuAdapter(OnNoteListener onNoteListener, Context context) {
        this.mOnNoteListener = onNoteListener;
        this.context = context;
    }

    @NonNull
    @Override
    public MenuAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_adapter_menu, parent, false);
        ViewHolder holder = new MenuAdapter.ViewHolder(view,mOnNoteListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MenuAdapter.ViewHolder holder, int position) {
        holder.asignarDatos(listaMenus.get(position));
    }

    @Override
    public int getItemCount() {
        return listaMenus.size();
    }


    public void setListaMenu(List<MenuEntity> listaMenus){
        this.listaMenus = listaMenus;
        notifyDataSetChanged();
    }



    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView nombreMenu;
        private AppCompatImageView eliminar;
        OnNoteListener onNoteListener;


        public ViewHolder(@NonNull View itemView, OnNoteListener onNoteListener) {
            super(itemView);
            nombreMenu = itemView.findViewById(R.id.nombreMenu_card);
            eliminar = itemView.findViewById(R.id.iconoBorrar);

            this.onNoteListener = onNoteListener;
            itemView.setOnClickListener(this);


        }

        public void asignarDatos(MenuEntity listaInfo) {
            nombreMenu.setText(listaInfo.getNombreMenuID());

            eliminar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AppDataBase dataBase = AppDataBase.getInstanceDataBase(context);
                    MenuDAO menuDAO = dataBase.menuDAO();
                    // Obtengo la lista por el id
                    String nombreMenu = listaInfo.getNombreMenuID();
                    MenuEntity menu = menuDAO.findById(nombreMenu);
                    //borro ese menu
                    menuDAO.delete(menu);
                    // Actualizo la lista
                    listaMenus=  menuDAO.getAll();
                    setListaMenu(listaMenus);

                }
            });
        }

        @Override
        public void onClick(View v) {
            onNoteListener.onNoteClick(getAdapterPosition());
        }
    }

    public interface  OnNoteListener{
        void onNoteClick (int position);
    }
}
