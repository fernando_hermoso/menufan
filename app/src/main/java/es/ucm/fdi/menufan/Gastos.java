package es.ucm.fdi.menufan;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

import es.ucm.fdi.menufan.dataBase.AppDataBase;
import es.ucm.fdi.menufan.dataBase.DAO.ListaCompraDAO;
import es.ucm.fdi.menufan.dataBase.DAO.PrecioDAO;
import es.ucm.fdi.menufan.dataBase.DAO.PrecioListaDAO;
import es.ucm.fdi.menufan.dataBase.Entities.ListaCompraEntity;
import es.ucm.fdi.menufan.dataBase.Entities.PrecioEntity;
import es.ucm.fdi.menufan.dataBase.Entities.PrecioListaEntity;

public class Gastos extends AppCompatActivity {
    BottomNavigationView navigation;
    LineChart lnChart;
    AppDataBase dataBase;
    ListaCompraDAO listaCompraDAO;
    PrecioDAO precioDAO;
    PrecioListaDAO precioListaDAO;
    private Spinner spinner;
    private List<ListaCompraEntity> listasCompras;
    private List<String> opciones;
    private  String listaSeleccioanda;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gastos);

        init();

    }





    private void init(){

        spinner = findViewById(R.id.spiner);
        //DataBase
        dataBase = AppDataBase.getInstanceDataBase(this.getApplicationContext());
        listaCompraDAO = dataBase.listaCompraDao();

        listasCompras = new ArrayList<>();
        opciones = new ArrayList<>();
        creaSpiner();

        try {
            listaSeleccioanda = spinner.getSelectedItem().toString();
        }catch (Exception e){
            Toast.makeText(this, "Añade un gasto a una lista primero",Toast.LENGTH_LONG).show();

        }





        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                listaSeleccioanda = spinner.getSelectedItem().toString();
                createChartBar();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                listaSeleccioanda = "Añada un precio a una lista";
            }
        });

        precioDAO = dataBase.precioDAO();
        precioListaDAO = dataBase.precioListaDAO();

        navigation = findViewById(R.id.menu_bottom_navigation);
        navigation.setSelectedItemId(R.id.menu_gastos);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.menu_gastos:
                        return true;
                    case R.id.menu_list:
                        startActivity(new Intent(getApplicationContext(), ListaCompras.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.menu_menu:
                        startActivity(new Intent(getApplicationContext(), Menus.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.menu_recetas:
                        startActivity(new Intent(getApplicationContext(), Recetas.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.menu_productos:
                        startActivity(new Intent(getApplicationContext(), Productos.class));
                        overridePendingTransition(0, 0);
                        return true;

                }
                return false;
            }
        });

        createChartBar();
    }


    private void creaSpiner(){



        listasCompras = listaCompraDAO.getAll();
        for(ListaCompraEntity l: listasCompras){
            opciones.add(l.getNombreListaID());
        }

        ArrayAdapter<String> adapterOpciones = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,opciones);
        spinner.setAdapter(adapterOpciones);

    }

    private void createChartBar() {
        lnChart = findViewById(R.id.chart);


        LineDataSet dataGasto = new LineDataSet(gasto(), "Gasto");
        dataGasto.setColor(Color.RED);


        ArrayList<String> labels = new ArrayList<String>();
        labels = fecha();

        LineData data = new LineData(dataGasto);

        lnChart.setData(data);
        lnChart.setDragEnabled(true);//puedes mover las graficas con el dedo
        lnChart.setDrawBorders(true);


        //  lnChart.setVisibleXRangeMaximum(3);
        float barSpace = 0.08f;
        float groupSpace = 0.44f;

        lnChart.invalidate();

    }


    // Crea las barras systolicas

    private ArrayList<Entry> gasto() {

        List<PrecioListaEntity> list = new ArrayList<>();
        list =  precioListaDAO.findById(listaSeleccioanda);



        ArrayList<Entry> gastolicList = new ArrayList<>();
        int cont = 0;
        for (PrecioListaEntity i : list) {
            int idPrecio = i.getPrecioId();

            PrecioEntity precioEntity = precioDAO.findById(idPrecio);

            gastolicList.add(new Entry(cont, precioEntity.getGasto().floatValue()));
            cont++;
        }

        return gastolicList;
    }

    private ArrayList<String> fecha() {
        List<PrecioListaEntity> list = new ArrayList<>();
        list =  precioListaDAO.findById(listaSeleccioanda);

        ArrayList<String> gastolicList = new ArrayList<>();
        int cont = 0;
        for (PrecioListaEntity i : list){

            int idPrecio = i.getPrecioId();

            PrecioEntity precioEntity = precioDAO.findById(idPrecio);

            gastolicList.add(precioEntity.getFecha());
            cont++;
        }

        return gastolicList;
    }

}