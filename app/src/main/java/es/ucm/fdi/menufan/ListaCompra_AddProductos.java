package es.ucm.fdi.menufan;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import es.ucm.fdi.menufan.dataBase.AppDataBase;
import es.ucm.fdi.menufan.dataBase.DAO.ListacompraProductoDAO;
import es.ucm.fdi.menufan.dataBase.DAO.ProductoDAO;
import es.ucm.fdi.menufan.dataBase.Entities.ListacompraProductoEntity;
import es.ucm.fdi.menufan.dataBase.Entities.ProductoEntity;

public class ListaCompra_AddProductos extends AppCompatActivity {
    Context context;
    TextView producto;
    Button guardar;

    String nombreLista;

    AppDataBase dataBase;
    ListacompraProductoDAO listacompraProductoDAO;
    ProductoDAO productoDAO;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_compra_add_productos);

        init();
    }


    private  void init(){

        context = this;
        producto = findViewById(R.id.productoNuevo);
        guardar = findViewById(R.id.AñadirProducto);

        dataBase = AppDataBase.getInstanceDataBase(this.getApplicationContext());
        listacompraProductoDAO = dataBase.listacompraProductoDAO();
        productoDAO = dataBase.productoDAO();


        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Comprobar di el nombre del producto esta vacio
                if(!producto.getText().toString().trim().equalsIgnoreCase("")){

                    // 1º Insertamos el producto en la entidad producto
                    ProductoEntity list = new ProductoEntity(producto.getText().toString().toUpperCase());
                    productoDAO.insert(list);
                    // 2º insertamos el nombre de la lista y del producto en la relacion
                       ListacompraProductoEntity listacompraProductoEntity = new ListacompraProductoEntity(nombreLista,producto.getText().toString(),1);
                       listacompraProductoDAO.insert(listacompraProductoEntity);

                       Intent intent = new Intent(context, Listacompra_Productos.class);
                       startActivity(intent);
                }else{
                    Toast.makeText(context, "Introduce un nombre para tu producto",Toast.LENGTH_LONG).show();
                }
            }
        });

        // Obtengo los datos del intent
        Intent intent = getIntent();
        //Obtener nombre de la Lista para meterlo en listaProductoEntity
        nombreLista = intent.getStringExtra("listaCompra_addProducto");


    }
}