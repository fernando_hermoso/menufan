package es.ucm.fdi.menufan.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import es.ucm.fdi.menufan.QuantityListener;
import es.ucm.fdi.menufan.R;
import es.ucm.fdi.menufan.dataBase.Entities.ProductoEntity;

public class AddProductoAdapter extends RecyclerView.Adapter<AddProductoAdapter.ViewHolder>{

    private List<ProductoEntity> lista ;
    private List<String> registrarProducto;
    QuantityListener   quantityListener;

    public AddProductoAdapter() {
    }

    public AddProductoAdapter(List<String> registrarProducto, QuantityListener quantityListener) {
        this.registrarProducto = registrarProducto;
        this.quantityListener = quantityListener;
    }



    @NonNull
    @Override
    public AddProductoAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_adapter_add_productos, parent, false);
        AddProductoAdapter.ViewHolder holder = new AddProductoAdapter.ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull AddProductoAdapter.ViewHolder holder, int position) {
        holder.asignarDatos(lista.get(position));
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public void setListaProductos(List<ProductoEntity> listaProducto){
        this.lista = listaProducto;
        notifyDataSetChanged();
    }

    public ProductoEntity getProducto (int pos){
        return lista.get(pos);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView nombre;
        CheckBox marcada;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            nombre = itemView.findViewById(R.id.producto);
            marcada =  itemView.findViewById(R.id.checkBox);

        }


        public void asignarDatos(ProductoEntity listaInfo) {
            nombre.setText(listaInfo.getNombreProductoID());
            marcada.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (marcada.isChecked()){
                        registrarProducto.add(listaInfo.getNombreProductoID());

                    }else {
                        registrarProducto.remove(listaInfo.getNombreProductoID());

                    }
                    quantityListener.arrayRegistroProductos(registrarProducto);
                }
            });

        }
    }
}
