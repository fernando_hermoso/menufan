package es.ucm.fdi.menufan;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import es.ucm.fdi.menufan.adapters.ListaProductosAdapter;
import es.ucm.fdi.menufan.adapters.ProductoAdapter;
import es.ucm.fdi.menufan.dataBase.AppDataBase;
import es.ucm.fdi.menufan.dataBase.DAO.ListacompraProductoDAO;
import es.ucm.fdi.menufan.dataBase.DAO.PrecioDAO;
import es.ucm.fdi.menufan.dataBase.DAO.PrecioListaDAO;
import es.ucm.fdi.menufan.dataBase.Entities.ListacompraProductoEntity;
import es.ucm.fdi.menufan.dataBase.Entities.PrecioEntity;
import es.ucm.fdi.menufan.dataBase.Entities.PrecioListaEntity;
import es.ucm.fdi.menufan.dataBase.Entities.ProductoEntity;

public class Listacompra_Productos extends AppCompatActivity {

    /*
    * CLASE QUE SE MUESTRA AL PULSAR A UNA LSITA DE LA COMPRA, EN ESTA TE MUESTRA SUS PRODUCTOS
    * */

    Button addProducto;
    TextView NombreListaCompra;
    Context context;
    ListaProductosAdapter listaProductosAdapter;
    RecyclerView recyclerViewProducto;
    List<ListacompraProductoEntity> listaProducto;

    AppDataBase dataBase;
    ListacompraProductoDAO listacompraProductoDAO;
    PrecioDAO precioDAO;
    PrecioListaDAO precioListaDAO;
    String  datoNOmbreLista;
    EditText gasto;
    Button addGasto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listacompra_productos);

        init();

    }

    @Override
    protected void onResume () {
        super.onResume();
        listaProducto = listacompraProductoDAO.findById(datoNOmbreLista);
        listaProductosAdapter.setListaProductos(listaProducto);
    }

    private void init(){
        datoNOmbreLista = getIntent().getStringExtra("NombreLista");
        context = this;
        listaProductosAdapter = new ListaProductosAdapter(this);
        addProducto = findViewById(R.id.botonFLotante);
        recyclerViewProducto = findViewById(R.id.listaCompra_productosRecycleview);
        recyclerViewProducto.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        recyclerViewProducto.setAdapter(listaProductosAdapter);
        listaProducto = new ArrayList<>();
        NombreListaCompra = findViewById(R.id.a);
        addProducto = findViewById(R.id.botonFLotante);

        gasto = findViewById(R.id.aniadirGasto);
        addGasto = findViewById(R.id.addGasto);
        addGasto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    // 1º Añadir el gasto en la Entidad gasto
                    double gas = Double.parseDouble(gasto.getText().toString());


                    String timeStamp = new SimpleDateFormat("yyyy/MM/dd").format(Calendar.getInstance().getTime());
                    PrecioEntity precioEntity = new PrecioEntity(gas, timeStamp );
                    precioDAO.insert(precioEntity);

                    // 2º Añadir el gasto asociado a la lista
                    List<PrecioEntity> a = precioDAO.getAll();
                    int cont = a.size();
                    PrecioListaEntity precioListaEntity = new PrecioListaEntity(a.get(cont-1).getPreciID(),datoNOmbreLista);
                    precioListaDAO.insert(precioListaEntity);

                    gasto.setText("");
                    Toast.makeText(context, "Se ha insertado correctamente el gasto",Toast.LENGTH_LONG).show();

                }catch (Exception e){
                    Toast.makeText(context, "Ha acurrido un problema al insertar el gasto",Toast.LENGTH_LONG).show();
                }




            }
        });

        //DataBase
        dataBase = AppDataBase.getInstanceDataBase(this.getApplicationContext());
        listacompraProductoDAO= dataBase.listacompraProductoDAO();
        precioDAO = dataBase.precioDAO();
        precioListaDAO = dataBase.precioListaDAO();

        NombreListaCompra.setText(datoNOmbreLista);

        addProducto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //meter funcionalidad de añair un producto segun lista
                Intent intent = new Intent(context, ListaCompra_AddProductos.class);
                intent.putExtra("listaCompra_addProducto",datoNOmbreLista);
                startActivity(intent);
            }
        });
    }
}