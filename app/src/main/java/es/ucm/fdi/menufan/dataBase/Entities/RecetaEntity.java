package es.ucm.fdi.menufan.dataBase.Entities;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;


@Entity( tableName = "Receta")
public class RecetaEntity {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "Receta")
    private String NombreRecetaID;

    @ColumnInfo(name = "Descripcion")
    private String Descripcion;



    public RecetaEntity() {
    }

    public RecetaEntity(@NonNull String nombreRecetaID) {
        NombreRecetaID = nombreRecetaID;
    }

    public RecetaEntity(@NonNull String nombreRecetaID, String descripcion) {
        NombreRecetaID = nombreRecetaID;
        Descripcion = descripcion;
    }

    @NonNull
    public String getNombreRecetaID() {
        return NombreRecetaID;
    }

    public void setNombreRecetaID(@NonNull String nombreRecetaID) {
        NombreRecetaID = nombreRecetaID;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }
}
