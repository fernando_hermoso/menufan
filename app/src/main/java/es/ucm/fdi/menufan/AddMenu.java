package es.ucm.fdi.menufan;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import es.ucm.fdi.menufan.adapters.RegistrosProductosListaAdapter;
import es.ucm.fdi.menufan.dataBase.AppDataBase;
import es.ucm.fdi.menufan.dataBase.DAO.MenuDAO;
import es.ucm.fdi.menufan.dataBase.DAO.RecetaMenuDAO;
import es.ucm.fdi.menufan.dataBase.Entities.MenuEntity;
import es.ucm.fdi.menufan.dataBase.Entities.RecetaMenuEntity;

public class AddMenu extends AppCompatActivity {

    private static final int REGISTER_RECETAS_REQUEST = 2;

    Context context;
    EditText nombreMenu;
    Button guardarMenu;

    RecyclerView recyclerView;
    Button floatingActionButton;

    AppDataBase dataBase;
    MenuDAO menuDAO;
    RecetaMenuDAO recetaMenuDAO;

    RegistrosProductosListaAdapter registroMenusAdapter;

    List<String> listaLocal;
    List<String>listaRegistroMenus;

    BottomNavigationView navigation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_menu);

        init();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REGISTER_RECETAS_REQUEST){
            if(resultCode == RESULT_OK){


                listaLocal = (List<String>) data.getSerializableExtra("listaRegistroRecetas");

                for(String i: listaLocal){

                    if (!buscarElemento(i)){
                        listaRegistroMenus.add(i);
                    }

                }
                registroMenusAdapter.setListaProductos(listaRegistroMenus);
            }else if(resultCode == RESULT_CANCELED){
                Log.d("Error", "Ha ocurridor error en cargar el registro de productos");
            }

        }

    }

    private Boolean buscarElemento (String nombreProducto){

        Boolean encontrado = false;
        int j = 0;
        while (!encontrado && j < listaRegistroMenus.size()){
            if (listaRegistroMenus.get(j).equals(nombreProducto)){
                encontrado = true;
            }
            j++;
        }
        return encontrado;
    }

    private void init(){
        context = this;
        nombreMenu = findViewById(R.id.menuNuevo);
        guardarMenu = findViewById(R.id.AñadirMenu);
        recyclerView = findViewById(R.id.RecyclerView);
        floatingActionButton = findViewById(R.id.botonFLotante);

        listaLocal = new ArrayList<>();
        listaRegistroMenus = new ArrayList<>();

        registroMenusAdapter = new RegistrosProductosListaAdapter(listaRegistroMenus);
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        recyclerView.setAdapter(registroMenusAdapter);

        dataBase = AppDataBase.getInstanceDataBase(this.getApplicationContext());
        menuDAO = dataBase.menuDAO();
        recetaMenuDAO = dataBase.recetaMenuDAO();

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AddRecetasLista.class);
                intent.putExtra("listaRegistroRecetas",(Serializable)listaLocal);
                startActivityForResult(intent,REGISTER_RECETAS_REQUEST);
            }
        });

        guardarMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Comprobar si campos NOmbre  vacios

                if(!nombreMenu.getText().toString().trim().equalsIgnoreCase("")){
                    MenuEntity men = new MenuEntity(nombreMenu.getText().toString());
                    menuDAO.insert(men);

                    //Añadir esas recetas a la tabla Recetas_Menu

                    for (int i = 0; i< listaRegistroMenus.size(); i++){
                        RecetaMenuEntity receta = new RecetaMenuEntity(nombreMenu.getText().toString(),listaRegistroMenus.get(i));
                        recetaMenuDAO.insert(receta);
                    }

                    Toast.makeText(context, "Menu añadido",Toast.LENGTH_LONG).show();
                    startActivity(new Intent(context, Menus.class));

                }else{

                    Toast.makeText(context, "Introduce un nombre  a tu Menu",Toast.LENGTH_LONG).show();
                }



            }
        });

        navigation = findViewById(R.id.menu_bottom_navigation);
        //navigation.setSelectedItemId(R.id.menu_menu);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.menu_gastos:
                        startActivity(new Intent(getApplicationContext(), Gastos.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.menu_list:
                        startActivity(new Intent(getApplicationContext(), ListaCompras.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.menu_menu:
                        startActivity(new Intent(getApplicationContext(), Menus.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.menu_recetas:
                        startActivity(new Intent(getApplicationContext(), Recetas.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.menu_productos:
                        startActivity(new Intent(getApplicationContext(), Productos.class));
                        overridePendingTransition(0, 0);
                        return true;

                }
                return false;
            }
        });

    }
}