package es.ucm.fdi.menufan.dataBase.Entities;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;


@Entity(tableName = "Producto")
public class ProductoEntity {

    @PrimaryKey
    @ColumnInfo(name = "producto")
    @NonNull
    private String nombreProductoID;

    @Ignore
    public ProductoEntity() {
    }

    public ProductoEntity(@NonNull String nombreProductoID) {
        this.nombreProductoID = nombreProductoID;
    }

    @NonNull
    public String getNombreProductoID() {
        return nombreProductoID;
    }



    public void setNombreProductoID(@NonNull String nombreProductoID) {
        this.nombreProductoID = nombreProductoID;
    }
}
