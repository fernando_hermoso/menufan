package es.ucm.fdi.menufan.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import es.ucm.fdi.menufan.R;
import es.ucm.fdi.menufan.dataBase.AppDataBase;
import es.ucm.fdi.menufan.dataBase.DAO.ListaCompraDAO;
import es.ucm.fdi.menufan.dataBase.Entities.ListaCompraEntity;

public class ListaCompraAdapter extends RecyclerView.Adapter<ListaCompraAdapter.ViewHolder>{

    private List<ListaCompraEntity> listaCompra = new ArrayList<>();
    private OnNoteListener mOnNoteListener;
    private Context context;


    public ListaCompraAdapter(OnNoteListener onNoteListener, Context context) {
        this.mOnNoteListener = onNoteListener;
        this.context = context;
    }

    public ListaCompraAdapter( Context context) {

        this.context = context;
    }


    @NonNull
    @Override
    public ListaCompraAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {//Enlazar el adaptafor Holder con el archivo xml de 1 item
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_adapter_lista_compra, parent, false);
        ViewHolder holder = new ViewHolder(view, mOnNoteListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ListaCompraAdapter.ViewHolder holder, int position) {//Encargado de la comunicacion entre nuestro adaptador y viewHoler
        holder.asignarDatos(listaCompra.get(position));
    }

    @Override
    public int getItemCount() {
        return listaCompra.size();
    }


    public void setListaCompra(List<ListaCompraEntity> listaCompra){
        this.listaCompra = listaCompra;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView nombreLista;
       // private Button eliminar;
        private AppCompatImageView eliminar;
        private AppCompatImageView favorito;
        private Boolean fav = false;
        OnNoteListener onNoteListener;

        public ViewHolder(@NonNull View itemView,  OnNoteListener onNoteListener) {
            super(itemView);
            nombreLista = itemView.findViewById(R.id.nombreLista_card);
            eliminar = itemView.findViewById(R.id.iconoBorrar);
            favorito = itemView.findViewById(R.id.iconofavorito);
           // eliminar.setVisibility(View.INVISIBLE);
            this.onNoteListener = onNoteListener;

            itemView.setOnClickListener(this);

        }

        public void asignarDatos(ListaCompraEntity listaInfo) {
            nombreLista.setText(listaInfo.getNombreListaID());

            //Instancio la BD y su DAO
            AppDataBase dataBase= AppDataBase.getInstanceDataBase(context);
            ListaCompraDAO listaCompraDAO = dataBase.listaCompraDao();


            // Obtengo la lista por el id
            String nombre = listaInfo.getNombreListaID();
            ListaCompraEntity listaCompraEntity = listaCompraDAO.findById(nombre);


            eliminar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    // Elimino de la lista la lista elegida
                    listaCompraDAO.delete(listaCompraEntity);
                    // Actualizo la lista
                    listaCompra = listaCompraDAO.getAll();
                    setListaCompra(listaCompra);
                }
            });

            // Falla aqui porque si elimino cuando quiere buscar para poner el icono, esta ya esta borrada por eso falla

            Boolean res = listaCompraEntity.getFavorito();
            if (res.equals(false)){
                favorito.setImageResource(R.drawable.ic_favorito_vacio);
            }else{
                favorito.setImageResource(R.drawable.ic_favorito_relleno);
            }


            favorito.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (listaCompraEntity.getFavorito().equals(false)){
                        ////  actualizo el campo favorito de ListaCompraEntity a true
                        listaCompraEntity.setFavorito(true);
                        listaCompraDAO.update(listaCompraEntity);
                        listaCompra = listaCompraDAO.findNoFavoritos();
                    }else{
                        listaCompraEntity.setFavorito(false);
                        listaCompraDAO.update(listaCompraEntity);
                        listaCompra = listaCompraDAO.findFavoritos();
                    }


                    // Actualizo la lista
                    setListaCompra(listaCompra);

                }
            });





        }

        @Override
        public void onClick(View v) {
            onNoteListener.onNoteClick(getAdapterPosition());
        }
    }

    public interface  OnNoteListener{
        void onNoteClick (int position);
    }
}


/*
*
*  favorito.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Instancio la BD y su DAO
                    AppDataBase dataBase= AppDataBase.getInstanceDataBase(context);
                    ListaCompraDAO listaCompraDAO = dataBase.listaCompraDao();

                    // Obtengo la lista por el id
                    String nombre = listaInfo.getNombreListaID();
                    ListaCompraEntity listaCompraEntity = listaCompraDAO.findById(nombre);

                    if (fav == false){

                        ////  actualizo el campo favorito de ListaCompraEntity a true
                        listaCompraEntity.setFavorito(Boolean.TRUE);
                        listaCompraDAO.update(listaCompraEntity);

                        // CAmbio al icono de estrella rellena
                        favorito.setImageResource(R.drawable.ic_favorito_relleno);
                        fav = true;

                    }else{

                         //  actualizo el campo favorito de ListaCompraEntity a false
                        listaCompraEntity.setFavorito(Boolean.FALSE);
                        listaCompraDAO.update(listaCompraEntity);

                        // CAmbio al icono de estrella vacia
                        // CAmbio al icono de estrella rellena
                        favorito.setImageResource(R.drawable.ic_favorito_vacio);
                        fav = false;
                    }

                    // Actualizo la lista
                    listaCompra = listaCompraDAO.getAll();
                    setListaCompra(listaCompra);
                }
            });
* */